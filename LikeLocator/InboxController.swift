//
//  InboxController.swift
//  LikeLocator
//
//  Created by MAC on 21/09/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit
import FTIndicator
import SDWebImage

class InboxController: UIViewController {
    @IBOutlet weak var inboxTableView: UITableView!
    var inboxArray : [InboxUser] = []
    var helloWorldTimer : Timer!
    var selectedData : InboxUser!
    var userID : NSNumber!
    @IBOutlet weak var  objAJProgressView = AJProgressView()
    @IBOutlet weak var loaderView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
         helloWorldTimer = Timer.scheduledTimer(timeInterval: 600.0, target: self, selector: #selector(InboxController.deleteChat), userInfo: nil, repeats: true)
        

        
        
        if let user = UserDefaults.standard.object(forKey: "User") as? NSDictionary{
            let uid : NSNumber = user["User_Id"] as! NSNumber
            userID = uid
        }
        inboxTableView.tableFooterView = UIView()
        self.loaderView.isHidden = true
        self.getInbox()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if inboxArray.count > 0{
            self.inboxTableView.reloadData()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
     @IBAction func backAction(_ sender: Any) {//Card Back Action
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func createLoader(){
        
        
        // Pass the color for the layer of progressView
        objAJProgressView?.firstColor = GlobalConstants.PINK_COLOR
        
        // If you  want to have layer of animated colors you can also add second and third color
        objAJProgressView?.secondColor = GlobalConstants.PINK_COLOR
        objAJProgressView?.thirdColor = GlobalConstants.PINK_COLOR
        
        // Set duration to control the speed of progressView
        objAJProgressView?.duration = 2.0
        
        // Set width of layer of progressView
        objAJProgressView?.lineWidth = 12
        
        //Set backgroundColor of progressView
        objAJProgressView?.bgColor =  UIColor.black.withAlphaComponent(0.3)
        
        //Get the status of progressView, if view is animating it will return true.
        _ = objAJProgressView?.isAnimating
        
        
    }
    
    func deleteChat()
    {
        NSLog("hello Delete")
    }
    
    func getInbox(){
        self.loaderView.isHidden = false
        self.objAJProgressView?.show()
        let param : [String : String] = ["To_Id":userID.stringValue]
        let urlString : String = URLConstants.baseUrl + URLConstants.inboxApi + param.convertDictionary()
        ServerData.getResponseFromURLWithBlock(urlString: urlString, completion: {(dataDictionary,sucess) in
            if sucess!{
                self.objAJProgressView?.hide()
                self.loaderView.isHidden = true
                if let status = dataDictionary!["Status"] as? Bool{
                    if status == true{
                        if let users = dataDictionary!["Chats"] as? NSArray{
                            for i in 0 ..< users.count {
                                let dict : NSDictionary = users[i] as! NSDictionary
                                let user : InboxUser = InboxUser.init(user: dict)
                                self.inboxArray.append(user)
                            }
                            self.inboxTableView.reloadData()
                        }
                    }else{
                        FTIndicator.showToastMessage("Right now you don't have chat with any user.")
                    }    }
                else{
                    self.objAJProgressView?.hide()
                    self.loaderView.isHidden = true
                }
               }else{
                self.objAJProgressView?.hide()
                 self.loaderView.isHidden = true
            }
            
        })
        
    }
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Chat"{
            let controller = segue.destination as! ChatController
            controller.selectedUser = selectedData
        }
     }

    
}

extension InboxController : UITableViewDataSource , UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return inboxArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InboxCell", for: indexPath) as! InboxCell
        cell.selectionStyle = .none
        let inboxData : InboxUser = self.inboxArray[indexPath.row]
        let imgUrl : String = inboxData.imageUrl
        let URLis : URL = URL(string : imgUrl)!
        
        let mask : NSNumber = inboxData.mask
         let stID : NSNumber = inboxData.startId
        
        cell.nameLabel.text = inboxData.name
        
        cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.width / 2
        cell.userImageView.clipsToBounds = true
        
        if (stID != self.userID && mask == 1) || (stID == self.userID && mask == 1) || (stID == self.userID && mask == 0){
            
            
            cell.userImageView.sd_setImage(with: URLis, placeholderImage: #imageLiteral(resourceName: "placeholder"), options: .retryFailed, completed: nil)
            cell.userImageView.contentMode = .scaleAspectFill
//            cell.userImageView.sd_setImage(with:  URLis, placeholderImage: UIImage(named : "placeholder"), options: SDWebImageOptions.highPriority, completed: { (img, err, cache, urlString) in
//                if img != nil{
//                    let maskedImage : UIImage = (img?.circleMasked)!
//                    cell.userImageView.image = maskedImage
//                }
//            })
        }else if stID != self.userID && mask == 0{
            cell.nameLabel.text = ""
            cell.userImageView.image = UIImage(named  : "mask")
        }
    
        if inboxData.status == 0{
            cell.seenLabel.isHidden = false
        }else{
            cell.seenLabel.isHidden = true
        }
       
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 82
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
         selectedData = self.inboxArray[indexPath.row]
        print(inboxArray[indexPath.row])
        selectedData.status = 1
        self.inboxArray[indexPath.row] = selectedData
        print(inboxArray[indexPath.row])

        self.performSegue(withIdentifier: "Chat", sender: self)
        
    }
}

