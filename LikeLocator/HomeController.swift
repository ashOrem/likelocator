//
//  HomeController.swift
//  LikeLocator
//
//  Created by ok on 9/19/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit
import FTIndicator
import SDWebImage
import CoreLocation
import GoogleMobileAds


let TestAdUnit = "/6499/example/native";

class HomeController: UIViewController {
 
    
    var counter : Int = 0
    
    // MARK: - Variabes & Outlets
    @IBOutlet weak var userCollector: UICollectionView!
    @IBOutlet weak var likeImage: UIImageView!
    @IBOutlet weak var advertismentView: UIView!
    @IBOutlet weak var advertismentInnerView: GADNativeAppInstallAdView!
    
    var imagesArray = [UIImage]()


      @IBOutlet weak var adImageSlider: CPImageSlider!

    
 @IBOutlet weak var installButton: UIButton!
    
         @IBOutlet weak var  objAJProgressView = AJProgressView()
        @IBOutlet weak var loaderView: UIView!
    
    @IBOutlet weak var distanceImage: UILabel!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var statusDot: UILabel!
    var currentIndex : Int = 0
    var viewHeight : CGFloat = 0
    var viewWidth : CGFloat = 0
    var definedSize : CGSize!
    var usersArray : [SecondUser] = []
    var userID : NSNumber!
    
    var adLoader : GADAdLoader!
    // MARK: - View_Life_Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let vedioOption : GADVideoOptions = GADVideoOptions()
        vedioOption.startMuted = true
        self.adLoader = GADAdLoader.init(adUnitID: TestAdUnit, rootViewController: self, adTypes: [.nativeAppInstall], options: [vedioOption])
        self.adLoader.delegate = self
      
        
        

        adImageSlider.images = imagesArray
        adImageSlider.delegate = self

        
        
        
        
        createLoader()
        
        statusDot.layer.cornerRadius = 6
        statusDot.clipsToBounds = true
        statusDot.isHidden = true
       
        let app : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        viewWidth = (app.window?.bounds.size.width)!
        viewHeight  = (app.window?.bounds.size.height)!
        definedSize = CGSize(width : viewWidth, height : viewHeight)
   
        if let user = UserDefaults.standard.object(forKey: "User") as? NSDictionary{
        
            let uid : NSNumber = user["User_Id"] as! NSNumber
            userID = uid
        }
        
        
        advertismentInnerView.layer.shadowOpacity = 0.2
        advertismentInnerView.layer.shadowOffset = CGSize(width: 3, height: 3)
        advertismentInnerView.layer.shadowRadius = 15.0
        advertismentInnerView.layer.shadowColor = UIColor.white.withAlphaComponent(0.7).cgColor
        
        
        advertismentView.frame = (app.window?.frame)!
        advertismentView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.view.addSubview(advertismentView)
        
        advertismentView.isHidden = true
        
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(HomeController.swipe))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        advertismentInnerView.addGestureRecognizer(swipeRight)
        
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(HomeController.swipe))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        advertismentInnerView.addGestureRecognizer(swipeLeft)
        
        
        let tapView = UITapGestureRecognizer(target: self, action: #selector(HomeController.swipe))
        advertismentView.addGestureRecognizer(tapView)
        
        
        if let counterInt = UserDefaults.standard.integer(forKey: "Counter") as? Int{
            counter = counterInt
        }
        
        
        
        nameLabel.text = ""
        distanceLabel.text = ""
        likeLabel.text = ""
        likeImage.alpha = 0
        distanceImage.alpha = 0
        nameLabel.layer.shadowOpacity = 0.8
        nameLabel.layer.shadowOffset = CGSize(width : 0, height : 0)
        distanceLabel.layer.shadowOpacity = 0.8
        distanceLabel.layer.shadowOffset = CGSize(width : 0, height : 0)
        likeLabel.layer.shadowOpacity = 0.8
        likeLabel.layer.shadowOffset = CGSize(width : 0, height : 0)
        
        self.backBtn.alpha = 0.4
        self.backBtn.isUserInteractionEnabled = false
        // Do any additional setup after loading the view.
        
        //checkLocation()
        
        let notificationName2 = Notification.Name("CStatus")
        NotificationCenter.default.addObserver(self, selector: #selector(HomeController.cStatusNotifcation(_:)), name: notificationName2, object: nil)
    }
    
    func swipe(){
        self.advertismentView.isHidden = true
    }
    
    func counterSaved(){
        counter = counter + 1
        UserDefaults.standard.set(counter, forKey: "Counter")
        UserDefaults.standard.synchronize()
    }
    
    
    
    func checkLocation(){
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.showLocationAlert()
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        } else {
            print("Location services are not enabled")
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.usersArray.count == 0{
            self.loaderView.isHidden = false
            self.updateLocation()
        }
        self.updateStatus()
    }
    
    
    func showLocationAlert(){
       /* let myAlert = UIAlertController(title: "LikeLocator wants to access your location", message: "to get your likes", preferredStyle: UIAlertControllerStyle.alert)
        let okaction = UIAlertAction(title: "Discard", style: UIAlertActionStyle.default, handler: { action in
         
        })
        let settingaction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { action in
            UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
        })
        myAlert.addAction(okaction)
        myAlert.addAction(settingaction)
        self.present(myAlert, animated: true, completion: nil)*/
    }
    
    
    
    
    
    @objc func cStatusNotifcation(_ application: NSNotification) {
        self.updateStatus()
    }
    
    override var prefersStatusBarHidden: Bool{
        return false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Actions_For_the_Controller
    @IBAction func backAction(_ sender: Any) {//Card Back Action
        if usersArray.count > 0{
            self.counterSaved()
            if counter < 30{
                if currentIndex > 0{
                    currentIndex = currentIndex - 1
                    userCollector.setContentOffset(CGPoint(x:  CGFloat(currentIndex) * viewWidth, y: 0), animated: true)
                }
                self.showDetails()
                self.checkButton_HideandShow()
            }else{
                counter = 0
                self.adLoader.load(GADRequest())
            }
        }
    }
    @IBAction func chatAction(_ sender: Any) {
        if usersArray.count > 0{
            self.performSegue(withIdentifier: "Chat", sender: self)
        }
    }
    @IBAction func nextAction(_ sender: Any) {//Card Next Action
        if usersArray.count > 0{
            self.counterSaved()
            if counter < 30{
                if currentIndex  < usersArray.count - 1 {
                    currentIndex = currentIndex + 1
                    userCollector.setContentOffset(CGPoint(x:  CGFloat(currentIndex) * viewWidth, y: 0), animated: true)
                }else{
                    currentIndex = 0
                    userCollector.setContentOffset(CGPoint(x:  CGFloat(currentIndex) * viewWidth, y: 0), animated: false)
                }
                self.showDetails()
                self.checkButton_HideandShow()
            }else{
                counter = 0
                self.adLoader.load(GADRequest())
            }
        }
    }
    @IBAction func reportUser(_ sender: Any){//Report a User
        if usersArray.count > 0{
            let alertController = UIAlertController(title: "Do you want to report this user?", message: "", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "CANCEL", style: .cancel) { action in
            }
            alertController.addAction(cancelAction)
            let OKAction = UIAlertAction(title: "REPORT", style: .default) { action in
                self.reportFunc()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true) {
            }
        }
    }
    @IBAction func blockAction(_ sender: Any) {//Block User
        if usersArray.count > 0{
            let structDic : SecondUser = usersArray[currentIndex]
            let urlString : String = URLConstants.baseUrl + URLConstants.blockApi
            let param : [String : Any] = ["From_Id":structDic.uid.stringValue,"To_Id":userID.stringValue]
            self.likeBlock(isBlocked: true, url: urlString, param: param as! [String : String])
        }
    }
    @IBAction func likeAction(_ sender: Any) {//Like User
        if usersArray.count > 0{
         let structDic : SecondUser = usersArray[currentIndex]
        let urlString : String = URLConstants.baseUrl + URLConstants.likeApi
        let param : [String : Any] = ["From_Id":structDic.uid.stringValue,"To_Id":userID.stringValue]
        self.likeBlock(isBlocked: false, url: urlString, param: param as! [String : String])
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Chat"{
            let controller = segue.destination as! ChatController
             let structDic : SecondUser = usersArray[currentIndex]
            let dict : NSDictionary = ["User_Id":structDic.uid,"UserName":structDic.name,"Image":structDic.imageUrl,"Started_Id":0,"Mask":0,"ChatStatus":0]
            let chatBox : InboxUser = InboxUser.init(user: dict)
            controller.selectedUser = chatBox
        }
        
    }
    
    // MARK: - Private Class Methods
    func reportFunc(){//Report API Call
        if usersArray.count > 0{
            let structDic : SecondUser = usersArray[currentIndex]
            let param : [String : Any] = ["From_Id": structDic.uid.stringValue,"To_Id":userID.stringValue]
            let urlString : String = URLConstants.baseUrl + URLConstants.reportApi + param.convertDictionary()
            ServerData.getResponseFromURL(urlString: urlString, loaderNeed: false) { (dataDictionary) in
                FTIndicator.showToastMessage("\(self.nameLabel.text!) Reported Successfully")
            }
        }
    }
    func likeBlock(isBlocked : Bool , url : String , param : [String : String]){//Like & Block API Call
        let apiUrl  = url + param.convertDictionary()
        ServerData.getResponseFromURL(urlString: apiUrl, loaderNeed: true) { (dataDictionary) in
            if let status = dataDictionary["Status"] as? NSNumber{
                if status == 0{
                    if let msg = dataDictionary["Message"] as? String{
                        FTIndicator.showToastMessage(msg)
                    }
                }else{
                    if isBlocked == false{
                        var user : SecondUser = self.usersArray[self.currentIndex]
                        var num : NSNumber =  user.likes
                        num = NSNumber(value: num.intValue + 1)
                        user.likes = num
                        self.usersArray[self.currentIndex] = user
                        self.likeLabel.text = "\(num)"
                        
                          FTIndicator.showToastMessage("\(self.nameLabel.text!)  Successfully Liked")
                    }else{
                     FTIndicator.showToastMessage("\(self.nameLabel.text!) Successfully Blocked")
                        self.nextAction(self)
                    }
                }
              }
            
        
        }
    }
    

    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let centerX = scrollView.contentOffset.x + scrollView.frame.size.width/2
        for cell in userCollector.visibleCells {
            
            var offsetX = centerX - cell.center.x
            if offsetX < 0 {
                offsetX *= -1
            }
            
            cell.transform = CGAffineTransform(scaleX: 1, y: 1)
            if offsetX > 50 {
                
                let offsetPercentage = (offsetX - 50) / view.bounds.width
                var scaleX = 1-offsetPercentage
                
                if scaleX < 0.8 {
                    scaleX = 0.8
                }
                cell.transform = CGAffineTransform(scaleX: scaleX, y: scaleX)
            }
        }
    }
    
    func indexPathIsValid(indexPath: NSIndexPath) -> Bool
    {
        let section = indexPath.section
        let row = indexPath.row
        
        let lastSectionIndex = numberOfSections(in: userCollector) - 1
        
        //Make sure the specified section exists
        if section > lastSectionIndex
        {
            return false
        }
       
        let rowCount =  self.userCollector.numberOfItems(inSection: indexPath.section) - 1
        
        return row <= rowCount
    }
    
    
    func checkButton_HideandShow(){//Hide & Show Back Button
        if currentIndex == 0{
            self.backBtn.alpha = 0.4
            self.backBtn.isUserInteractionEnabled = false
        }
        else {
            self.backBtn.alpha = 1.0
            self.backBtn.isUserInteractionEnabled = true
        }
    }
    func showDetails(){//Show Details Of User on Screen
        if self.usersArray.count > 0{
            let person : SecondUser = self.usersArray[currentIndex]
            let like : NSNumber = person.likes
            self.likeLabel.text = like.stringValue
            self.nameLabel.text = person.name
            let dist : NSNumber = person.dic
            let distance : Double  = dist.doubleValue
            let distString : String = String(format: "%.2f Km", distance)
            self.distanceLabel.text = distString
        }
    }
    
    
    func createLoader(){
        
        
        // Pass the color for the layer of progressView
        objAJProgressView?.firstColor = GlobalConstants.PINK_COLOR
        
        // If you  want to have layer of animated colors you can also add second and third color
        objAJProgressView?.secondColor = GlobalConstants.PINK_COLOR
        objAJProgressView?.thirdColor = GlobalConstants.PINK_COLOR
        
        // Set duration to control the speed of progressView
        objAJProgressView?.duration = 2.0
        
        // Set width of layer of progressView
        objAJProgressView?.lineWidth = 12
        
        //Set backgroundColor of progressView
        objAJProgressView?.bgColor =  UIColor.black.withAlphaComponent(0.3)
        
        //Get the status of progressView, if view is animating it will return true.
        _ = objAJProgressView?.isAnimating
        
        
    }
    

    func getUsers(){//Get All the Users from server
        if let lat = UserDefaults.standard.object(forKey: "Lat") as? CGFloat{
            if let lng = UserDefaults.standard.object(forKey: "Lng") as? CGFloat{
                let param : [String : Any] = ["User_Id":userID.stringValue,"Lat":lat,"Lng":lng]
                let urlString : String = URLConstants.baseUrl + URLConstants.userLoadApi + param.convertDictionary()
            ServerData.getResponseFromURLWithBlock(urlString: urlString, completion: {(dataDictionary,sucess) in
                    if sucess!{
                        
                        if let status = dataDictionary!["Status"] as? Bool{
                            if status == true{
                                if let users = dataDictionary!["users"] as? NSArray{
                                    for i in 0 ..< users.count {
                                        let dict : NSDictionary = users[i] as! NSDictionary
                                        let user : SecondUser = SecondUser.init(card: dict)
                                        self.usersArray.append(user)
                                    }
                                    self.showDetails()
                                    self.likeImage.alpha = 1
                                    self.distanceImage.alpha = 1
                                    self.userCollector.reloadData()
                                    self.updateToken()
                                    //Use show() and hide() to manage progressView
                                    self.objAJProgressView?.hide()
                                    self.loaderView.isHidden = true
                                }
                            }
                            
                        }else{
                            
                            self.objAJProgressView?.hide()
                            self.loaderView.isHidden = true
                            FTIndicator.showToastMessage("Users Not Found")
                            
                        }
                        
                        
                        
                    }else{
                        self.objAJProgressView?.hide()
                        self.loaderView.isHidden = true
                    }
                    
                    })
                
               
        }else{
            //Use show() and hide() to manage progressView
            objAJProgressView?.hide()
            self.loaderView.isHidden = true

        }
        }else{
            objAJProgressView?.hide()
            self.loaderView.isHidden = true
            
        }}
    
    func updateLocation(){
       self.objAJProgressView?.show()
       if let lat = UserDefaults.standard.object(forKey: "Lat"){
            if let lng = UserDefaults.standard.object(forKey: "Lng"){
                let param : [String : Any] = ["User_Id":userID.stringValue,"lat":"\(lat)","lng":"\(lng)"]
                let urlString : String = URLConstants.baseUrl + URLConstants.locationUpdate + param.convertDictionary()
          ServerData.getResponseFromURLWithBlock(urlString: urlString, completion: {(data,sucess) in
                if sucess!{
                    self.getUsers()
                }else{
                    self.loaderView.isHidden = true
                  self.objAJProgressView?.hide()
            }
                    
                })
               
            }
       }else{
        self.loaderView.isHidden = true
            self.objAJProgressView?.hide()
        }

    }
    
    func updateStatus(){
        let param  = ["To_Id": userID.stringValue]
        let urlString : String = URLConstants.baseUrl + URLConstants.readStatus + param.convertDictionary()
        ServerData.getResponseFromURL(urlString: urlString, loaderNeed: false) { (dataDictionary) in
            if  let status = dataDictionary["Status"] as? NSNumber{
                if status == 1{
                     if  let cstatus = dataDictionary["ChatStatus"] as? NSNumber{
                        if cstatus == 1{
                            self.statusDot.isHidden = false
                        }else{
                            self.statusDot.isHidden = true
                        }
                    }
                }else{
                        self.statusDot.isHidden = true
                }
            }else{
                   self.statusDot.isHidden = true
            }
        }
    }
    
    func updateToken(){//Update a token when user enters
       if let token = UserDefaults.standard.object(forKey: "DToken") as? String{
            let param : [String : Any] = ["User_Id":userID,"Token":token,"Type":"2"]
            let urlString : String = URLConstants.baseUrl + URLConstants.deviceApi + param.convertDictionary()
            ServerData.getResponseFromURL(urlString: urlString, loaderNeed: false) { (dataDictionary) in
            }
       }
    }
}

extension HomeController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
 
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return usersArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Image", for: indexPath as IndexPath)
        
        let structDic : SecondUser = usersArray[indexPath.row]
        let imgView : UIImageView = cell.viewWithTag(10) as! UIImageView
        let urlString : String = structDic.imageUrl
        let imageURL : URL = URL(string : urlString)!
        
        imgView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "backImage"),options: SDWebImageOptions.highPriority, completed: { (img, error, cacheType, imageURL) in
            imgView.image = img?.resizeImageWith(newSize: self.definedSize)
            imgView.contentMode = .scaleAspectFill
        })
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width : viewWidth ,height: viewHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
   
    
}

extension HomeController : CPSliderDelegate{
    func sliderImageTapped(slider: CPImageSlider, index: Int) {
        
    }
    
}

extension HomeController :  GADNativeAppInstallAdLoaderDelegate , GADVideoControllerDelegate {
    func adLoaderDidFinishLoading(_ adLoader: GADAdLoader) {
        
    }
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: GADRequestError) {
        
    }
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAppInstallAd: GADNativeAppInstallAd) {
        
        
        imagesArray = [UIImage]()
        self.advertismentView.isHidden = false
        
        
        advertismentInnerView.nativeAppInstallAd = nativeAppInstallAd
        
        // Populate the app install ad view with the app install ad assets.
        // Some assets are guaranteed to be present in every app install ad.
        (advertismentInnerView.headlineView as! UILabel).text = nativeAppInstallAd.headline
        (advertismentInnerView.iconView as! UIImageView).image = nativeAppInstallAd.icon?.image
        (advertismentInnerView.bodyView as! UILabel).text = nativeAppInstallAd.body
  
        (advertismentInnerView.callToActionView as! UIButton).setTitle(
            nativeAppInstallAd.callToAction, for: UIControlState.normal)
 
        if nativeAppInstallAd.videoController.hasVideoContent(){
            self.adImageSlider.isHidden = true
            advertismentInnerView.mediaView?.isHidden = false
            nativeAppInstallAd.videoController.delegate = self
        }else{
            self.adImageSlider.isHidden = false
            advertismentInnerView.mediaView?.isHidden = true
            let art = nativeAppInstallAd.images! as NSArray
            for i in 0 ..< art.count {
                let gadImage : GADNativeAdImage = art[i] as! GADNativeAdImage
                let image : UIImage = gadImage.image!
                imagesArray.append(image)
            }
            if imagesArray.count == 1{
                adImageSlider.autoSrcollEnabled = false
                adImageSlider.enableArrowIndicator =  false
                adImageSlider.enablePageIndicator = false
                adImageSlider.enableSwipe = false
                adImageSlider.allowCircular = false
            }else{
                adImageSlider.autoSrcollEnabled = true
                adImageSlider.enableArrowIndicator =  true
                adImageSlider.enablePageIndicator = true
                adImageSlider.enableSwipe = true
                adImageSlider.allowCircular = true
            }
            print(imagesArray)
            self.adImageSlider.images = imagesArray
        }
        
        (advertismentInnerView.callToActionView as! UIButton).isUserInteractionEnabled = false

        


    }
    func videoControllerDidEndVideoPlayback(_ videoController: GADVideoController) {
        
    }
}

