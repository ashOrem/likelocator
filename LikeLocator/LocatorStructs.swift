//
//  LocatorStructs.swift
//  LikeLocator
//
//  Created by MAC on 21/09/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit

////////////All Users
struct SecondUser {
    var name: String
    var dic: NSNumber
    var imageUrl: String
    var sex: Bool
    var likes: NSNumber
    var uid: NSNumber
    
    init(card : NSDictionary )
    {
        uid =  card["id"]! as! NSNumber
        name =  card["username"]! as! String
        dic =  card["dic"]! as! NSNumber
        imageUrl  =  card["imageUrl"]! as! String
        sex =  card["sex"]! as! Bool
        likes = card["likes"]! as! NSNumber
    }
}

////////////All Inbox Users
struct InboxUser {
    var name: String
    var imageUrl: String
    var startId: NSNumber
    var mask: NSNumber
    var uid: NSNumber
    var status: NSNumber
    
    init(user : NSDictionary )
    {
        uid =  user["User_Id"]! as! NSNumber
        name =  user["UserName"]! as! String
        imageUrl  =  user["Image"]! as! String
        startId =  user["Started_Id"]! as! NSNumber
        mask = user["Mask"]! as! NSNumber
        status = user["ChatStatus"]! as! NSNumber
    }
}

////////////Current Location
struct LocationCoordinates  {
    var Lattitude: Double
    var Longitude: Double
  
    init(lat : Double , lng : Double) {
        self.Lattitude = lat
        self.Longitude = lng
    }

}

////////////All Inbox Users
struct ChatBox {
    var msg: String
    var img: String?
    var date: String
    var friendImage: String
    var userImage: String
    var isImage: Bool
    var isFriend: Bool
    var recentImage: UIImage?
    init(chat : NSDictionary )
    {
        let myID : NSNumber = UserDefaults.standard.object(forKey: "UID") as! NSNumber
        let msgSenderID : NSNumber = chat["To_Id"]! as! NSNumber
        if msgSenderID == myID{
            isFriend = false
        }else{
            isFriend = true
        }
        
        if let img = chat["img"] as? UIImage{
            recentImage = img
        }else{
            img =  chat["img"]! as? String
        }

        msg =  chat["Text"]! as! String
        date =  chat["Date"]! as! String
        friendImage  =  chat["FromImage"]! as! String
        userImage  =  chat["ToImage"]! as! String
        isImage = true
        if msg.characters.count > 0{
            isImage = false
        }

    }
}


////////////All Inbox Users
struct MySelf {
    var name: String
    var imageUrl: String
    var uid: NSNumber
    var password: String
    var gender: Bool
    var likes: NSNumber
    init(name : String , imageUrl : String , uid : NSNumber , password : String,gender: Bool,likes : NSNumber) {
        self.likes = likes
        self.name = name
        self.imageUrl = imageUrl
        self.uid = uid
        self.password = password
        self.gender = gender
    }
}


class GlobalME {
    static let sharedGlobal = GlobalME()
    var member:MySelf!
}
