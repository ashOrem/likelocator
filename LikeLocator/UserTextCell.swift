//
//  UserTextCell.swift
//  LikeLocator
//
//  Created by MAC on 25/09/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit

class UserTextCell: UITableViewCell {
         @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var msgLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        userImageView.layer.cornerRadius = 20
        userImageView.clipsToBounds = true
        backView.layer.cornerRadius = 4
        backView.clipsToBounds = true
        

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
  
}
