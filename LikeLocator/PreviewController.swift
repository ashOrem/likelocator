//
//  PreviewController.swift
//  LikeLocator
//
//  Created by MAC on 26/09/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit
import SDWebImage

class PreviewController: UIViewController {
    var imageSelected : UIImage?
    var imageURLString : String?
    @IBOutlet weak var previewerImageView: UIImageView!
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        previewerImageView.image = #imageLiteral(resourceName: "placeholder")
        if imageSelected != nil{
            previewerImageView.image = imageSelected
        }else{
            if let url = URL(string : imageURLString!){
                          previewerImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"),options: SDWebImageOptions.retryFailed, completed: nil)
            }
        }
        previewerImageView.contentMode = .scaleAspectFit
        previewerImageView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapImage(sender:)))
        tapGesture.numberOfTapsRequired = 1
        previewerImageView.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    func tapImage(sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
