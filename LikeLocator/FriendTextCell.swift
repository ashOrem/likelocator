//
//  FriendTextCell.swift
//  LikeLocator
//
//  Created by MAC on 25/09/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit

class FriendTextCell: UITableViewCell {
    @IBOutlet weak var msgLabel: UILabel!
    @IBOutlet weak var friendImageView: UIImageView!
    @IBOutlet weak var backView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        friendImageView.layer.cornerRadius = 20
        friendImageView.clipsToBounds = true
        backView.layer.cornerRadius = 4
        backView.clipsToBounds = true
        
        
        let tip = UILabel(frame: CGRect(x:-10, y: 0, width: 10, height: 10))
        print(tip)
        tip.backgroundColor = UIColor.red
        backView.addSubview(tip)

        
        
//        let path = UIBezierPath()
//        path.move(to: CGPoint(x: 0, y: 0))
//        path.addLine(to: CGPoint(x: 10, y: 0))
//        path.addLine(to: CGPoint(x: 10, y: 10))
//
//        // Draw arrow
//        path.addLine(to: CGPoint(x: 0, y: 0))
//        path.addLine(to: CGPoint(x: 0, y: 10))
//        path.addLine(to: CGPoint(x: 10, y: 10))
//
//        path.addLine(to: CGPoint(x: 0, y: 10))
//        path.close()
//
//        let shape = CAShapeLayer()
//        //shape.backgroundColor = UIColor.blue.cgColor
//        shape.fillColor = UIColor.orange.cgColor
//        shape.path = path.cgPath
//        backView.layer.addSublayer(shape)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
