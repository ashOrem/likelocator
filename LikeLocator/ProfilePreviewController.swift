//
//  ProfilePreviewController.swift
//  LikeLocator
//
//  Created by MAC on 26/09/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit
import SDWebImage
import FTIndicator
class ProfilePreviewController: UIViewController {
    var picker:UIImagePickerController?=UIImagePickerController()
  
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func camAction(_ sender: Any) {
        self.openCamera()
    }
    @IBAction func gallaryAction(_ sender: Any) {
          self.openGallary()
    }
    
    func openGallary()
    {
        picker!.allowsEditing = false
        picker?.delegate = self
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(picker!, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            picker!.allowsEditing = false
            picker?.delegate = self
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            picker!.cameraCaptureMode = .photo
            present(picker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProfilePreviewController :  UIImagePickerControllerDelegate , UINavigationControllerDelegate ,UIPopoverControllerDelegate{
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        picker.dismiss(animated: true, completion: nil)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var img = info[UIImagePickerControllerOriginalImage] as? UIImage
        img = img?.resizeImageWith(newSize:CGSize(width : self.view.frame.size.width,height : self.view.frame.size.height))
        let data = UIImageJPEGRepresentation(img!, 1)
     
        UserDefaults.standard.set(data, forKey: "ImgData")
        UserDefaults.standard.synchronize()
        
        picker.dismiss(animated: true) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    
    
}
