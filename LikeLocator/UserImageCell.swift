//
//  UserImageCell.swift
//  LikeLocator
//
//  Created by MAC on 25/09/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit

class UserImageCell: UITableViewCell {
    @IBOutlet weak var msgImageView: UIImageView!
    @IBOutlet weak var backView: UIView!
     @IBOutlet weak var userImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        userImageView.layer.cornerRadius = 20
        msgImageView.layer.cornerRadius = 5
       // msgImageView.layer.borderWidth = 2
       // msgImageView.layer.borderColor =   UIColor(red : 19/255.0, green:159/255.0 , blue :212/255.0 , alpha : 1.0).cgColor
        userImageView.clipsToBounds = true
        msgImageView.clipsToBounds = true

        backView.layer.cornerRadius = 4
        backView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
