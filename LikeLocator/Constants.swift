//
//  Constants.swift
//  DigiLabs
//
//  Created by Apple on 21/08/17.
//  Copyright © 2017 VINSAM. All rights reserved.
//

import UIKit


struct GlobalConstants {
    static let DEFAULT_COLOR_BLUE: UIColor = UIColor(red: 23/255.0, green: 180/255.0, blue: 191/255.0, alpha: 1.0)
    static let COLLECTION_COLOR2: UIColor = UIColor(red: 120/255.0, green: 198/255.0, blue: 112/255.0, alpha: 1.0)
    static let COLLECTION_COLOR3: UIColor = UIColor(red: 71/255.0, green: 134/255.0, blue: 63/255.0, alpha: 1.0)
    static let COLLECTION_COLOR1: UIColor = UIColor(red: 149/255.0, green: 202/255.0, blue: 203/255.0, alpha: 1.0)
    static let BLUE_COLOR: UIColor = UIColor(red: 91/255.0, green: 194/255.0, blue: 236/255.0, alpha: 1.0)
    static let BACK_COLOR: UIColor = UIColor(red: 242/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0)
       static let PINK_COLOR: UIColor = UIColor(red: 253/255.0, green: 62/255.0, blue: 84/255.0, alpha: 1.0)
}


struct URLConstants  {
    //App Base URL
    static let baseUrl = "http://likelocator.azurewebsites.net/api/"
    
    //LoginApi
    static let loginApi = "users/login"
    //Parameters : {"password":"password","username":"username"}
    
    //RegisterApi
    static let registerUpdateApi = "users/registration"
    /*Parameters : {"Email":"bb","Password":"123","id":"110","Sex":false} pass id in case of updating profile else remain blank for the registration*/
    
    static let userImageUploadApi = "users/postUserImage"
    //Parameters : picture == File
    
    static let userLoadApi = "users/loadPerson?"
    //Parmeters : User_Id=948&Lat=30.7082092&Lng=76.7028468

    static let userImageApi = "users/getUserImage?"
    //Parameters : userId=3017
    
    static let likeApi = "users/likeUser?"
    //Parameters : From_Id=523&To_Id=549 , where From_Id is mine id & To_Id is the second persons Id
    
    static let blockApi = "users/blockUser?"
    //Parameters : To_Id=362&From_Id=549

    static let chatMessageApi = "Chats/SendMsgImg"
    //Parameters : "From_Id=534&To_Id=549&Text=Hi&myFile=choosenImage"
    
    static let inboxApi = "Chats/Inbox?"
    //Parameters : "To_Id=950"
    
    static let maskApi = "Chats/SetMask?"
    /*Parameters : From_Id=549&To_Id=523&Mask=1 //// Mask will be 0 & 1
     Mask = 1 -> Show Profile || if Mask == 0 & Start Id == MineID
     Mask = 2 -> Hide Profile || if Mask == 0 & Start Id =! MineID*/
    
    static let deviceApi = "Users/SetDeviceToken?"
    //Parameters : User_Id=549&Token=cdfjbfnsfsdfjh&Type=2
    
    static let deleteChatApi = "Chats/DeleteChat?"
    //Parameters : To_Id=549&From_Id=523
    
    static let getProfielApi = "Chats/GetProfile?"
    //Parametrs : To_Id=949

    static let reportApi = "Chats/Report?"
    //Parameters : From_Id=948&To_Id=949
    
    static let readStatus = "Chats/ReadStatus?"
    //Parameters : To_Id=950
    
    static let postImage = "users/postUserImage"
    //Parameters : picture = File Type , Auth Header = Username Password
    
    static let locationUpdate = "Users/SetCountry?"
    //Parameters : User_Id = 946 , lat =30.7082738 ,  lng = 76.7027946
    
    static let getChatMessages = "Chats/GetMassages?"
    //Parameters : To_Id = 13772 , From_Id =950
    
}

struct DefinedStrings  {
    static let internetError = "Internet Connectivity is Not Available"
    static let serverError = "Server Not Responding"
    static let loaderText = "Loading..."
    static let validRequired = "Please Enter Required Fields"
    static let validEmail = "Please Enter Valid Email"
}






//Login - plsceholder , pink profile image , ImagePIcker profile image space left ,

/// profile - white edit remove , textfields white appear

// Block api - skip that from collection one forward

//If image return nil placeholder must show -----  home screen


// When not data check for emty data
// HOme reload after network connectivity
// Chat bottom textfied
// Aspect Fit image Preview / back button



