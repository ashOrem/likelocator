//
//  FriendImageCell.swift
//  LikeLocator
//
//  Created by MAC on 25/09/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit

class FriendImageCell: UITableViewCell {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var msgImageView: UIImageView!
    @IBOutlet weak var friendImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        friendImageView.layer.cornerRadius = 20
        msgImageView.layer.cornerRadius = 5
       // msgImageView.layer.borderWidth = 2
       // msgImageView.layer.borderColor = UIColor.white.cgColor
        friendImageView.clipsToBounds = true
        msgImageView.clipsToBounds = true
//        backView.layer.cornerRadius = 4
//        backView.clipsToBounds = true
      //  backView.addPikeOnView(side: .Left)
        
       
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
    
}
