//
//  Helper.m
//  LikeLocator
//
//  Created by MAC on 29/09/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

#import "Helper.h"

@implementation Helper


+(NSArray *)uncommonArray : (NSArray *)newArray oldArray :(NSArray *)oldArray{
    
    NSArray *smallArray = [oldArray subarrayWithRange:NSMakeRange(0, 1)];
    
    NSMutableSet *set1 = [NSMutableSet setWithArray: smallArray];
    NSSet *set2 = [NSSet setWithArray: oldArray];
    [set1 intersectSet: set2];
    NSArray *resultArray = [set1 allObjects];
    return resultArray;
}

@end
