//
//  WebberController.swift
//  LikeLocator
//
//  Created by MAC on 01/11/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit
import FTIndicator

class WebberController: UIViewController {

    @IBOutlet weak var webber: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        FTIndicator.showProgress(withMessage: "Loading...")
        
        let urlString : URL = URL(string : "http://likelocator.azurewebsites.net/PrivacyPolicy.html")!
        let request : URLRequest = URLRequest(url : urlString)
        self.webber.loadRequest(request)

        // Do any additional setup after loading the view.
    }
   
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WebberController : UIWebViewDelegate{
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        FTIndicator.dismissProgress()
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        FTIndicator.dismissProgress()
    }
}
