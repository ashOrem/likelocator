//
//  ChatController.swift
//  LikeLocator
//
//  Created by MAC on 21/09/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit
import SDWebImage
import FTIndicator
import IQKeyboardManagerSwift

class ChatController: UIViewController  {
    var keyPadAppeared : Bool = false
    @IBOutlet weak var galWidth: NSLayoutConstraint!
    @IBOutlet weak var textviewBottom: NSLayoutConstraint!
    var onlyOnce : Bool = true
    var meSender : Bool = false
    var firstTimeArray : Int = 0
    var firstTime : Bool = true
    var sendImage : UIImage?
    @IBOutlet weak var camView2: UIView!
    @IBOutlet weak var camView: UIView!
    @IBOutlet weak var adminLabel: UILabel!
    @IBOutlet weak var maskBtn: UIButton!
    var isAdmin : Bool = false
    var chatStartID : NSNumber!
     var chatMask : NSNumber!
       var friendMasked : Bool = false
    var picker:UIImagePickerController?=UIImagePickerController()
    var selectedUser : InboxUser!
    var mask : NSNumber!
    var userID : NSNumber!
    var chatTimer :Timer!
    var chatDateString : String!
    @IBOutlet weak var textInputBar : UITextField!
    var selectedImage  : UIImage?
    var selectedURL : String?
    
    @IBOutlet weak var txtFieldLeading: NSLayoutConstraint!
    @IBOutlet weak var globeWidth: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIView!
    
    // This is how we observe the keyboard position
    @IBOutlet weak var blockBtn: UIButton!
    @IBOutlet weak var adminImageView: UIImageView!

    
    @IBOutlet weak var nameLabel: UILabel!
    @IBAction func blockUserAction(_ sender: UIButton) {
        let param : [String : Any] = ["From_Id":selectedUser.uid,"To_Id":userID.stringValue]
        let urlString : String = URLConstants.baseUrl + URLConstants.blockApi + param.convertDictionary()
        sender.alpha = 0.5
        sender.isUserInteractionEnabled = false
        ServerData.getResponseFromURL(urlString: urlString, loaderNeed: true) { (dataDictionary) in
            FTIndicator.showToastMessage(dataDictionary["Message"] as! String)
        }
    }
    
    
    
    // This is also required
    override var canBecomeFirstResponder: Bool {
        return true
    }
    @IBAction func openCamView(_ sender: Any) {
        self.view.endEditing(true)
        camView.alpha = 0
        camView2.alpha = 0
        camView.isHidden = false
        camView2.isHidden = false
        UIView.animate(withDuration: 0.3) {
            self.camView.alpha = 0.8
            self.camView2.alpha = 1
        }
    }
    
    func tapCamView(){
        UIView.animate(withDuration: 0.3, animations: {
            self.camView.alpha = 0
            self.camView2.alpha = 0
        }) { (finished) in
            self.camView.isHidden = finished
            self.camView2.isHidden = finished
        }
    }
    
    
    @IBAction func camAction(_ sender: Any) {
        openCamera()
    }
    
      @IBAction func gallaryAction(_ sender: Any) {
        openGallary()
    }
    
    
    func openGallary()
    {
        picker!.allowsEditing = false
        picker?.delegate = self
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(picker!, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            picker!.allowsEditing = false
            picker?.delegate = self
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            picker!.cameraCaptureMode = .photo
            present(picker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    var chatArray : [ChatBox] = []
    @IBOutlet weak var chatTableView: UITableView!
    
   
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.stopTimer()
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
    }
    func startTimer()
    {
        if chatTimer == nil {
            chatTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(ChatController.getChat), userInfo: nil, repeats: true)
        }
    }
    
    func stopTimer()
    {
        if chatTimer != nil {
            chatTimer!.invalidate()
            chatTimer = nil
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adminLabel.isHidden = true
        adminImageView.isHidden = true
        bottomView.alpha = 0
        
        bottomView.backgroundColor = UIColor.black

        self.view.backgroundColor = UIColor.black
      
        
        if selectedUser == nil{
            isAdmin = true
            blockBtn.isHidden = true
        }
        
        chatDateString = Date().dashedStringFromDate()
        
        

        if let user = UserDefaults.standard.object(forKey: "User") as? NSDictionary{
            userID = user["User_Id"] as! NSNumber
            UserDefaults.standard.set(userID, forKey: "UID")
            UserDefaults.standard.synchronize()
        }
        
        self.chatTableView.register(UINib(nibName: "FriendImageCell", bundle: nil), forCellReuseIdentifier: "FriendImageCell")
        
        self.chatTableView.register(UINib(nibName: "FriendTextCell", bundle: nil), forCellReuseIdentifier: "FriendTextCell")
        
        self.chatTableView.register(UINib(nibName: "UserImageCell", bundle: nil), forCellReuseIdentifier: "UserImageCell")
        
        self.chatTableView.register(UINib(nibName: "UserTextCell", bundle: nil), forCellReuseIdentifier: "UserTextCell")
        
        self.chatTableView.backgroundColor = UIColor.clear
        self.chatTableView.estimatedRowHeight = 80
        self.chatTableView.rowHeight = UITableViewAutomaticDimension
        self.chatTableView.tableFooterView = UIView()
        self.deleteChat()
        
        
        self.camView2.isHidden = true
        self.camView.isHidden = true
        self.camView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapCamView))
        self.camView.addGestureRecognizer(tapGesture)
        
 
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func deleteChat(){
        if isAdmin == true{
            self.getChat()
        }
        else{
            let param = ["To_Id":userID.stringValue,"From_Id":selectedUser.uid.stringValue]
            let urlString : String = URLConstants.baseUrl + URLConstants.deleteChatApi
            
            ServerData.postResponseFromURL(urlString: urlString, showIndicator: false, params: param, completion: { (dataDictionary) in
                self.getChat()
            })
       
        
        }
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.camView.isHidden = true
        self.camView2.isHidden = true
        IQKeyboardManager.sharedManager().enable = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
    }

    
    
    
    func configureInputBar(maskImage : UIImage?) {
        if keyPadAppeared == false{
            bottomConstraint.constant = 54
        }
        bottomView.alpha = 1
        if maskImage == nil || self.isAdmin == true
        {
            if self.isAdmin == true{
                self.galWidth.constant = 0
            }else{
                self.galWidth.constant = 35
            }
            self.globeWidth.constant = 0
            if isAdmin == true{
                adminLabel.isHidden = false
                adminImageView.isHidden = false
            }else{
                adminLabel.isHidden = true
                adminImageView.isHidden = true
            }
        }else{
            self.galWidth.constant = 35
            self.maskBtn.setImage(maskImage, for: .normal)
            //self.txtFieldLeading.constant = 8
            self.globeWidth.constant = 35
        }
     
    }
    
 
    
    func keyboardWillShow(notification: NSNotification) {
        keyPadAppeared = true
        if let userInfo = notification.userInfo {
            let frame = userInfo[UIKeyboardFrameEndUserInfoKey] as! CGRect
            bottomConstraint.constant =   frame.size.height + 54
            textviewBottom.constant = frame.size.height
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        keyPadAppeared = false
            bottomConstraint.constant = 54
             textviewBottom.constant = 0
    }
    
    func updateTableFrame(){
        let oldLastCellIndexPath = IndexPath(row: self.chatArray.count-1, section: 0)
        self.chatTableView.scrollToRow(at: oldLastCellIndexPath, at: .bottom, animated: false)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0)
        {
            self.textInputBar.resignFirstResponder()
        }
    }

    

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        
        if segue.identifier == "Preview"{
            let controller = segue.destination as! PreviewController
           
            if selectedURL != nil{
                controller.imageURLString = selectedURL
            }else{
                 controller.imageSelected = selectedImage
            }
         
        }
        
     }
 
    
    @IBAction func maskAction(_ sender: Any) {
        if isAdmin == false{
            if self.mask == 0{
                let param = ["From_Id":selectedUser.uid.stringValue,"To_Id":userID.stringValue,"Mask":"1"]
                let urlString : String = URLConstants.baseUrl + URLConstants.maskApi + param.convertDictionary()
                ServerData.getResponseFromURL(urlString: urlString, loaderNeed: true, completion: { (dataDictionary) in
                    self.mask = 1
                    self.configureInputBar(maskImage: #imageLiteral(resourceName: "globe"))
                    self.chatTableView.reloadData()
                })
            }
        }
    }
    @IBAction func sendMessageAction(_ sender: Any) {
        self.stopTimer()
        var FID : NSNumber = 1
        if isAdmin == false{
            FID = selectedUser.uid
        }
        
        self.meSender = true
        let str : String = textInputBar.text!.Trim()
        if str.characters.count > 0{
        let urlString : String = URLConstants.baseUrl + URLConstants.chatMessageApi
            print(urlString)
        let param : [String : String] = ["From_Id":FID.stringValue,"To_Id":userID.stringValue,"Text":str]
            print(param)
        var fromString : String = ""
        var toString : String = ""
        if self.chatArray.count > 0{
            let chat : ChatBox = self.chatArray[0]
            
            fromString = chat.friendImage
            toString = chat.userImage
        }

        textInputBar.text = ""
    

        ServerData.uploadTaskFor(URLString: urlString, parameters: param  , file: nil, showIndicator: false) { (dataDictionary) in
            print(dataDictionary)
            
            if self.chatArray.count == 0{
                self.firstTime = true
            }else{
                self.firstTime = false
            }
            
            if let status = dataDictionary["Status"] as? NSNumber{
                if status == 0{
                    if let msg = dataDictionary["Message"] as? String{
                        FTIndicator.showToastMessage(msg)
                    }
                }
                self.getChat()
                /*else{
                    let chat : NSDictionary = ["From_Id":FID,"To_Id":self.userID,"Text":str,"img":"","Date":self.chatDateString,"ToImage":toString,"FromImage":fromString]
                    let box : ChatBox = ChatBox.init(chat: chat)
                    self.chatArray.append(box)
                    let oldLastCellIndexPath = IndexPath(row: self.chatArray.count-1, section: 0)
                    self.chatTableView.insertRows(at: [oldLastCellIndexPath], with: .right)
                    self.chatTableView.scrollToRow(at: oldLastCellIndexPath, at: .bottom, animated: false)
                    
                    self.firstTimeArray = self.firstTimeArray + 1

                }*/
            }
            self.startTimer()
            self.meSender = false
        }
        }else{
            FTIndicator.showToastMessage("Can't Send Empty Message")
        }
    }
    
    
    
    func getChat(){
        var param  : [String : String] = [:]
        if isAdmin == true{
             param  = ["To_Id" : userID.stringValue , "From_Id" : "1"]
        }else{
            
            param  = ["To_Id" : userID.stringValue , "From_Id" : selectedUser.uid.stringValue]
        }
        
        let urlString = URLConstants.baseUrl + URLConstants.getChatMessages + param.convertDictionary()
        var loader : Bool = false
        if self.onlyOnce == true {
            loader = true
            self.onlyOnce = false
        }
        
        
        ServerData.getResponseFromURL(urlString: urlString, loaderNeed: loader) { (dataDictionary) in
            print(dataDictionary)
            
            if let status = dataDictionary["Status"] as? NSNumber{
                if status == 1{
                    if let chats = dataDictionary["Chats"] as? NSArray{
                      
                        var newMessage : Bool = false
                        if self.chatArray.count > 0{
                            if self.chatArray.count < chats.count{
                                newMessage = true
                            }
                        }
                       
                        self.chatStartID = dataDictionary["Started_Id"] as! NSNumber
                        self.chatMask = dataDictionary["Mask"] as! NSNumber
                        
                        var maskImage : UIImage = #imageLiteral(resourceName: "mask")
                        maskImage  =  maskImage.maskWithColor(color: UIColor.white)!
                        
             
                        
                        if self.isAdmin == false{
                            if self.chatStartID == self.userID && self.chatMask == 0{
                                self.nameLabel.text = self.selectedUser.name.uppercased()
                                self.mask = 0// Started by me , uski profile mere ko dikhe gi meri nahi
                            }else if (self.chatStartID != self.userID && self.chatMask == 1)  ||  (self.chatStartID == self.userID && self.chatMask == 1){
                                self.nameLabel.text = self.selectedUser.name.uppercased()
                                self.mask = 1
                            }else if self.chatStartID != self.userID && self.chatMask == 0{
                                self.nameLabel.text = ""
                                self.mask = 1
                                self.friendMasked = true
                                self.nameLabel.text = ""
                            }
                                if self.mask == 0{
                                    self.configureInputBar(maskImage: maskImage)
                                }else{
                                    self.configureInputBar(maskImage: #imageLiteral(resourceName: "globe"))
                                }
                            
                        }else{
                                self.configureInputBar(maskImage: nil)
                        }
                        
                        
                        if chats.count > 0{
                            if newMessage == true || self.chatArray.count == 0{
                                
                                let chater : NSMutableArray = NSMutableArray(array : chats)
                                if newMessage == true{
                                    for i in 0 ..< self.chatArray.count{
                                        print(i)
                                        if chater.count > 0{
                                            chater.removeObject(at: 0)
                                        }
                                    }
                                }
                                
                                for i in 0 ..< chater.count {
                                    let dic = chater[i] as! NSDictionary
                                    let chaty : ChatBox = ChatBox.init(chat: dic)
                                    self.chatArray.append(chaty)
                                    
                                    let oldLastCellIndexPath = IndexPath(row: self.chatArray.count-1, section: 0)
                                    self.chatTableView.beginUpdates()
                                    self.chatTableView.insertRows(at: [oldLastCellIndexPath], with: .fade)
                                    self.chatTableView.endUpdates()
                                }
                                DispatchQueue.main.async {
                                    let oldLastCellIndexPath = IndexPath(row: self.chatArray.count-1, section: 0)
                                    self.chatTableView.scrollToRow(at: oldLastCellIndexPath, at: .bottom, animated: false)
                                }
                            }
                        }
                        
                        
                        
                        
                    }else{
                        self.configureInputBar(maskImage: nil)
                    }
                }else{
                    self.configureInputBar(maskImage: nil)
                }
            }
            
            
            self.startTimer()
  
         
           // self.getChat()
        }
    }
    

    
    @IBAction func backAction(_ sender: Any) {//Card Back Action
        self.navigationController?.popViewController(animated: true)
    }
}

extension ChatController : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.bottomConstraint.constant = 54
        textviewBottom.constant = 0

        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if chatArray.count > 0{
            let oldLastCellIndexPath = IndexPath(row: self.chatArray.count-1, section: 0)
            self.chatTableView.scrollToRow(at: oldLastCellIndexPath, at: .bottom, animated: false)
        }
    }
}

extension ChatController : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let chat : ChatBox = chatArray[indexPath.row]
        
        
        
        if chat.isFriend == true{
            if chat.isImage == true{
                let cell:FriendImageCell = tableView.dequeueReusableCell(withIdentifier: "FriendImageCell", for: indexPath) as! FriendImageCell
                cell.backgroundColor = UIColor.clear
                let urlString : String = chat.img!
                if let imageURL : URL = URL(string : urlString){
                    cell.msgImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "backImage"),options: SDWebImageOptions.retryFailed, completed: nil)
                }
                if  self.friendMasked == true{
                    cell.friendImageView.image = UIImage(named : "mask")!
                }else{
                    let furlString : String = chat.friendImage
                    if let imageURL : URL = URL(string : furlString){
                        cell.friendImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder"),options: SDWebImageOptions.retryFailed, completed: nil)
                    }
                    
                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapProfileImage(_:)))
                    cell.friendImageView.isUserInteractionEnabled = true
                    cell.friendImageView.addGestureRecognizer(tapGesture)
                }
                
 
                
                
                let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(self.tapImage(_:)))
                cell.msgImageView.isUserInteractionEnabled = true
                cell.msgImageView.addGestureRecognizer(tapGesture2)
                
                
                cell.selectionStyle = .none

                return cell
            }else{
                let cell:FriendTextCell = tableView.dequeueReusableCell(withIdentifier: "FriendTextCell", for: indexPath) as! FriendTextCell
                cell.backgroundColor = UIColor.clear
                if isAdmin == true{
                    cell.friendImageView.image = #imageLiteral(resourceName: "admin")
                }else{
                    if  self.friendMasked == true{
                        cell.friendImageView.image = UIImage(named : "mask")!
                    }else{
                        let urlString : String = chat.friendImage
                        if let imageURL : URL = URL(string : urlString){
                            cell.friendImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder"),options: SDWebImageOptions.retryFailed, completed: nil)
                        }
                        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapProfileImage(_:)))
                        cell.friendImageView.isUserInteractionEnabled = true
                        cell.friendImageView.addGestureRecognizer(tapGesture)
                    }
                }
        
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapProfileImage(_:)))
                cell.friendImageView.isUserInteractionEnabled = true
                cell.friendImageView.addGestureRecognizer(tapGesture)
                
                
                cell.msgLabel.text = chat.msg
                cell.selectionStyle = .none

                return cell
            }
        }else{
            if chat.isImage == true{
                let cell:UserImageCell = tableView.dequeueReusableCell(withIdentifier: "UserImageCell", for: indexPath) as! UserImageCell
                cell.backgroundColor = UIColor.clear
                
                if chat.recentImage !=  nil{
                    cell.msgImageView.image = chat.recentImage
                }else{
                    let urlString : String = chat.img!
                    if let imageURL : URL = URL(string : urlString){
                        cell.msgImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "backImage"),options: SDWebImageOptions.retryFailed, completed: nil)
                    }
                }
                if  self.mask == 0{
                    cell.userImageView.image = UIImage(named : "mask")!
                }else{
                    let myurlString : String = chat.userImage
                    if let imageURL : URL = URL(string : myurlString){
                        cell.userImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder"),options: SDWebImageOptions.retryFailed, completed: nil)
                    }
                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapProfileImage(_:)))
                    cell.userImageView.isUserInteractionEnabled = true
                    cell.userImageView.addGestureRecognizer(tapGesture)
                }

                

                
                
                let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(self.tapImage(_:)))
                cell.msgImageView.isUserInteractionEnabled = true
                cell.msgImageView.addGestureRecognizer(tapGesture2)
                
                cell.selectionStyle = .none

                return cell
            }else{
                let cell:UserTextCell = tableView.dequeueReusableCell(withIdentifier: "UserTextCell", for: indexPath) as! UserTextCell
                cell.backgroundColor = UIColor.clear
                cell.msgLabel.text = chat.msg
                
                if  self.mask == 0{
                    cell.userImageView.image = UIImage(named : "mask")!
                }else{
                    let myurlString : String = chat.userImage
                    if let imageURL : URL = URL(string : myurlString){
                        cell.userImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder"),options: SDWebImageOptions.retryFailed, completed: nil)
                    }
                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapProfileImage(_:)))
                    cell.userImageView.isUserInteractionEnabled = true
                    cell.userImageView.addGestureRecognizer(tapGesture)
                
                }
                
 
                
                
       
                
                
                
                cell.selectionStyle = .none
                return cell
            }
        }
    }
    
    func tapImage(_ sender: UIGestureRecognizer) {
        let touch = sender.location(in: chatTableView)
        if let indexPath = chatTableView.indexPathForRow(at: touch){
        let chat : ChatBox = chatArray[indexPath.row]
 
        if chat.msg.characters.count > 0{
            return
        }
        if (chat.recentImage != nil){
            self.selectedImage = chat.recentImage
            self.selectedURL = nil
            self.performSegue(withIdentifier: "Preview", sender: self)
        }else{
            self.selectedURL = chat.img
            self.selectedImage = nil
            self.performSegue(withIdentifier: "Preview", sender: self)
        }
        }
    }
    func tapProfileImage(_ sender: UIGestureRecognizer) {
        let touch = sender.location(in: chatTableView)
        if let indexPath = chatTableView.indexPathForRow(at: touch){
            let chat : ChatBox = chatArray[indexPath.row]
          
            
         
            if chat.isFriend{
                if isAdmin == true  || friendMasked == true{
                    return
                }
                self.selectedURL = chat.friendImage
            }else{
                self.selectedURL = chat.userImage
            }

                self.selectedImage = nil
                self.performSegue(withIdentifier: "Preview", sender: self)
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
   
    func uploadImage(){
        
        let urlString : String = URLConstants.baseUrl + URLConstants.chatMessageApi
        let fileData : UIImage? = selectedImage
        let textString : String = ""
        let param : [String : String] = ["From_Id":self.selectedUser.uid.stringValue ,"To_Id":self.userID.stringValue,"Text":textString]
        var fromString : String = ""
        var toString : String = ""
        if self.chatArray.count > 0{
            let chat : ChatBox = self.chatArray[0]
            fromString = chat.friendImage
            toString = chat.userImage
        }
        
        
        if self.chatArray.count == 0{
            self.firstTime = true
        }else{
            self.firstTime = false
        }
        
        self.firstTimeArray = self.firstTimeArray + 1
        
        
        self.meSender = true
        FTIndicator.showProgress(withMessage: "Uploading...", userInteractionEnable: false)
        ServerData.uploadTaskFor(URLString: urlString, parameters: param  , file: fileData, showIndicator: true) { (dataDictionary) in
            
            FTIndicator.dismissProgress()
            if let status = dataDictionary["Status"] as? NSNumber{
                if status == 0{
                    if let msg = dataDictionary["Message"] as? String{
                        FTIndicator.showToastMessage(msg)
                        
                    }
                    
                }
                else{
                    let chat : NSDictionary = ["From_Id":self.selectedUser.uid,"To_Id":self.userID,"Text":textString,"img":fileData!,"Date":self.chatDateString,"ToImage":toString,"FromImage":fromString]
                    let box : ChatBox = ChatBox.init(chat: chat)
                    self.chatArray.append(box)
                    
                    let oldLastCellIndexPath = IndexPath(row: self.chatArray.count-1, section: 0)
                    self.chatTableView.insertRows(at: [oldLastCellIndexPath], with: .left)
                    self.chatTableView.scrollToRow(at: oldLastCellIndexPath, at: .bottom, animated: false)
                }
                
            }
            
            self.startTimer()
            self.meSender = false
        }
        
    }
    
    func showAlert(){
        let myAlert = UIAlertController(title: "Do you want to send this image?", message: "", preferredStyle: UIAlertControllerStyle.alert)
        let okaction = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.default, handler: { action in
            
        })
        let settingaction = UIAlertAction(title: "SEND", style: UIAlertActionStyle.default, handler: { action in
            
            if self.selectedImage != nil{
                self.uploadImage()
            }
        })
        myAlert.addAction(okaction)
        myAlert.addAction(settingaction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    
}
extension ChatController :  UIImagePickerControllerDelegate , UINavigationControllerDelegate ,UIPopoverControllerDelegate{
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        picker.dismiss(animated: true, completion: nil)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
            self.stopTimer()
        var imageSelected = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.selectedImage = imageSelected?.resizeImageWith(newSize: CGSize(width : self.view.frame.size.width, height :self.view.frame.size.height))
        picker.dismiss(animated: true) {
            self.showAlert()
        }
        
        
    }
    
    
    
    
}

//https://stackoverflow.com/questions/30685163/compare-2-arrays-and-list-the-differences-swift
