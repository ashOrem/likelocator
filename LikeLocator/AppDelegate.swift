//pep3ry@abv.bg
//


//pep2ry@gmail.com
//Like1234


//
//  AppDelegate.swift
//  LikeLocator
//
//  Created by ok on 9/19/17.
//  Copyright © 2017 Praveen. All rights reserved.
//
//dfdgdfgdfgdfg
//qwerty
///dawindersingh.orem
import UIKit
import UserNotifications
import CoreLocation
import LocationRequestManager
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import IQKeyboardManagerSwift
import GoogleMobileAds


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate ,UNUserNotificationCenterDelegate, MessagingDelegate{
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("MY DToken : ----- \(fcmToken)")
        UserDefaults.standard.set(fcmToken, forKey: "DToken")
        UserDefaults.standard.synchronize()
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
         UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
        let notificationName = Notification.Name("CStatus")
        NotificationCenter.default.post(name: notificationName, object: nil)
    }
    
    let locationRequestManager: LocationRequestManager = LocationRequestManager()
    var basicRequest:LocationRequest?
    var window: UIWindow?


//    func userNotificationCenter(_ center: UNUserNotificationCenter,
//                                willPresent notification: UNNotification,
//                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
//    {
//        completionHandler(.alert)
//    }
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
 
        
        self.basicRequest = LocationRequest{(currentLocation:CLLocation?,error: Error?)->Void in
            if currentLocation != nil{
                let lat : Double = (currentLocation?.coordinate.latitude)!
                let lng : Double = (currentLocation?.coordinate.longitude)!
                
                UserDefaults.standard.set(lat, forKey: "Lat")
                UserDefaults.standard.synchronize()
                UserDefaults.standard.set(lng, forKey: "Lng")
                UserDefaults.standard.synchronize()
                
            }

            
            
//            let savedLocation : LocationCoordinates = LocationCoordinates.init(lat: lat, lng: lng)
//            let archiveData = NSKeyedArchiver.archivedData(withRootObject: savedLocation)
//            let ud = UserDefaults.standard
//            ud.set(  archiveData  ,forKey: "Location")
//            ud.synchronize()
            
            print("\(self.basicRequest!.status) - \(currentLocation) - \(error)")
        }
        locationRequestManager.addRequest(request: self.basicRequest!)
        locationRequestManager.performRequests()
        UIApplication.shared.statusBarStyle  = .lightContent
        
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        FirebaseApp.configure()
        
        GADMobileAds.configure(withApplicationID: "ca-app-pub-8939304857266204~7881977887")

   
        IQKeyboardManager.sharedManager().enable = true
        
//        if #available(iOS 10, *) {
//            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: { (granted, error) in })
//            application.registerForRemoteNotifications()
//        } else {
//            let notificationSettings = UIUserNotificationSettings(types: [.badge, .alert, .sound], categories: nil)
//            UIApplication.shared.registerUserNotificationSettings(notificationSettings)
//            UIApplication.shared.registerForRemoteNotifications()
//        }
        
        // FirebaseApp.configure()
        if (UserDefaults.standard.object(forKey: "Login") as? Bool) != nil{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "nav")
            window?.rootViewController = controller
        }else{
            let storyboard = UIStoryboard(name: "Register", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "LoginController")
            window?.rootViewController = controller
        }
        

        
        return true
    }
    
    

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print(token)
        Messaging.messaging().apnsToken = deviceToken
//        UserDefaults.standard.set(token, forKey: "DToken")
//        UserDefaults.standard.synchronize()
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
        let notificationName = Notification.Name("CStatus")
        NotificationCenter.default.post(name: notificationName, object: nil)
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
         application.applicationIconBadgeNumber = 0
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
         application.applicationIconBadgeNumber = 0
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }

    // MARK: - Core Data stack



}

