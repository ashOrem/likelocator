//
//  LocatorExtensions.swift
//  LikeLocator
//
//  Created by ok on 9/20/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit


extension String {
    
    //To check text field or String is blank or not
    var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: CharacterSet.whitespaces)
            return trimmed.isEmpty
        }
    }
    
    //Validate Email
    var isEmail: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}", options: .caseInsensitive)
            return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil
        } catch {
            return false
        }
    }
    
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
    
    //Trim Leading & Trailing Spaces
    func Trim() -> String {
            let trimmedString = self.trimmingCharacters(in: .whitespacesAndNewlines)

            return trimmedString
        }
}

extension Dictionary{
    func convertDictionary() -> String{
        let mutableString : NSMutableString = ""
        for (key, value) in self {
            print("\(key): \(value)")
            let str : String = "\(key)=" + "\(value)"
            if mutableString == ""{
                mutableString.append(str)
                
            }else{
                mutableString.append("&\(str)")
            }
        }
        return String(mutableString)
    }
}

extension UITextField {
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.black.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.white.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}

extension UIImage{
    func resizeImageWith(newSize: CGSize) -> UIImage {
        
        let scale = newSize.height / self.size.height
        let newWidth = self.size.width * scale
        UIGraphicsBeginImageContext(CGSize(width : newWidth, height : newSize.height))
        self.draw(in: CGRect(x:0,y: 0, width:newWidth,height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
