//
//  InboxCell.swift
//  LikeLocator
//
//  Created by MAC on 21/09/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit

class InboxCell: UITableViewCell {

        @IBOutlet weak var seenLabel: UILabel!
        @IBOutlet weak var nameLabel: UILabel!
        @IBOutlet weak var userImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        userImageView.layer.cornerRadius = userImageView.frame.size.width / 2.0
        userImageView.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
