//
//  ChatController.swift
//  LikeLocator
//
//  Created by MAC on 21/09/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit
import SDWebImage
import IQKeyboardManagerSwift
import ALTextInputBar
import FTIndicator
class ChatController: UIViewController  {
    @IBOutlet weak var adminLabel: UILabel!
    var isAdmin : Bool = false
    var picker:UIImagePickerController?=UIImagePickerController()
    var selectedUser : InboxUser!
    var mask : NSNumber!
    var userID : NSNumber!
    var chatDateString : String!
    let textInputBar = ALTextInputBar()
    var selectedImage  : UIImage?
    var selectedURL : String?
    let keyboardObserver = ALKeyboardObservingView()
    // This is how we observe the keyboard position
    @IBOutlet weak var blockBtn: UIButton!
    @IBOutlet weak var adminImageView: UIImageView!
    override var inputAccessoryView: UIView? {
        get {
            return keyboardObserver
        }
    }
    
    @IBAction func blockUserAction(_ sender: UIButton) {
        let param : [String : Any] = ["From_Id":selectedUser.uid,"To_Id":userID.stringValue]
        let urlString : String = URLConstants.baseUrl + URLConstants.blockApi + param.convertDictionary()
        sender.alpha = 0.5
        sender.isUserInteractionEnabled = false
        ServerData.getResponseFromURL(urlString: urlString, loaderNeed: true) { (dataDictionary) in
            FTIndicator.showToastMessage(dataDictionary["Message"] as! String)
        }
    }
    
    
    
    // This is also required
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    
    @IBAction func camAction(_ sender: Any) {
        self.textInputBar.textView.resignFirstResponder()
        let alertController = UIAlertController(title: "Select Image From :", message: "", preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
        }
        alertController.addAction(cancelAction)
        let camAction = UIAlertAction(title: "Camera", style: .default) { action in
            self.openCamera()
        }
        alertController.addAction(camAction)
        
        let gallaryAction = UIAlertAction(title: "Gallary", style: .default) { action in
            self.openGallary()
        }
        alertController.addAction(gallaryAction)
        self.present(alertController, animated: true) {
        }
        
    }
    
    func openGallary()
    {
        picker!.allowsEditing = false
        picker?.delegate = self
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(picker!, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            picker!.allowsEditing = false
            picker?.delegate = self
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            picker!.cameraCaptureMode = .photo
            present(picker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    var chatArray : [ChatBox] = []
    @IBOutlet weak var chatTableView: UITableView!
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if selectedUser == nil{
            isAdmin = true
            blockBtn.isHidden = true
        }
        
        chatDateString = Date().dashedStringFromDate()
        
        print("Ashwani Your Friend : -----  \(selectedUser)")
        

        if let user = UserDefaults.standard.object(forKey: "User") as? NSDictionary{
            userID = user["User_Id"] as! NSNumber
            UserDefaults.standard.set(userID, forKey: "UID")
            UserDefaults.standard.synchronize()
        }
        
        self.chatTableView.register(UINib(nibName: "FriendImageCell", bundle: nil), forCellReuseIdentifier: "FriendImageCell")
        
        self.chatTableView.register(UINib(nibName: "FriendTextCell", bundle: nil), forCellReuseIdentifier: "FriendTextCell")
        
        self.chatTableView.register(UINib(nibName: "UserImageCell", bundle: nil), forCellReuseIdentifier: "UserImageCell")
        
        self.chatTableView.register(UINib(nibName: "UserTextCell", bundle: nil), forCellReuseIdentifier: "UserTextCell")
        
        self.chatTableView.backgroundColor = UIColor.clear
        self.chatTableView.estimatedRowHeight = 80
        self.chatTableView.rowHeight = UITableViewAutomaticDimension
        self.chatTableView.tableFooterView = UIView()
        self.getChat()
        
        
        
        
        configureInputBar()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardFrameChanged(notification:)), name: NSNotification.Name(rawValue: ALKeyboardFrameDidChangeNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        IQKeyboardManager.sharedManager().enable = false
    }
    override func viewWillLayoutSubviews(){
        super.viewWillLayoutSubviews()
        textInputBar.frame.size.width = view.bounds.size.width
    }
    func configureInputBar() {
        let globeButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        let sendButton = UIButton(frame: CGRect(x: 6, y: 0, width: 44, height: 44))
     let gallaryButton = UIButton(frame: CGRect(x: 56, y: 0, width: 44, height: 44))
        gallaryButton.setTitleColor(UIColor.blue, for: .normal)
        globeButton.setImage(#imageLiteral(resourceName: "globe"), for: .normal)
        sendButton.setImage(#imageLiteral(resourceName: "send"), for: .normal)
        gallaryButton.setTitle("", for: .normal)
        
        gallaryButton.addTarget(self, action: #selector(ChatController.camAction(_:)), for: .touchUpInside)

        globeButton.addTarget(self, action: #selector(ChatController.maskAction(_:)), for: .touchUpInside)

        sendButton.addTarget(self, action: #selector(ChatController.sendMessageAction(_:)), for: .touchUpInside)

        
        
        gallaryButton.titleLabel?.font = UIFont(name : "FontAwesome" , size : 30)
        let rightView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 44))
        rightView.addSubview(sendButton)
        rightView.addSubview(gallaryButton)
        rightView.backgroundColor = UIColor.clear
        
        keyboardObserver.isUserInteractionEnabled = false
        
        textInputBar.showTextViewBorder = true
        
        if isAdmin == true{
            adminLabel.isHidden = false
            adminImageView.isHidden = false

            sendButton.frame =  CGRect(x: 0, y: 0, width: 44, height: 44)
            textInputBar.rightView =  sendButton
             textInputBar.alwaysShowRightButton = false
            
        }else{
            adminLabel.isHidden = true
            adminImageView.isHidden = true

            textInputBar.leftView = globeButton
            textInputBar.rightView = rightView
             textInputBar.alwaysShowRightButton = true
        }
        

        textInputBar.frame = CGRect(x: 0, y: view.frame.size.height - textInputBar.defaultHeight, width: view.frame.size.width, height: textInputBar.defaultHeight)
        textInputBar.backgroundColor = UIColor(white: 0.95, alpha: 1)
        textInputBar.keyboardObserver = keyboardObserver
       
        view.addSubview(textInputBar)
    }
    
    func keyboardFrameChanged(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let frame = userInfo[UIKeyboardFrameEndUserInfoKey] as! CGRect
            textInputBar.frame.origin.y = frame.origin.y
            
            print("Frame Change = \(frame.origin.y)")
        }
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let frame = userInfo[UIKeyboardFrameEndUserInfoKey] as! CGRect
            textInputBar.frame.origin.y = frame.origin.y
             print("Show = \(frame.origin.y)")
            bottomConstraint.constant =   frame.size.height
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let frame = userInfo[UIKeyboardFrameEndUserInfoKey] as! CGRect
            textInputBar.frame.origin.y = frame.origin.y
             print("Hide = \(frame.origin.y)")
            bottomConstraint.constant = 44
        }
    }
    
    func updateTableFrame(){
        let oldLastCellIndexPath = IndexPath(row: self.chatArray.count-1, section: 0)
        self.chatTableView.scrollToRow(at: oldLastCellIndexPath, at: .bottom, animated: false)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0)
        {
            print("up")
            self.textInputBar.textView.resignFirstResponder()
        }
        else
        {
            print("down")
            //here you can dismiss keyboard.
        }
    }

    
   /* func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            print("HELLO")
            if bottomConstraint.constant == 0{
                bottomConstraint.constant = keyboardSize.height
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if bottomConstraint.constant != 0{
            bottomConstraint.constant = 0
        }
    }*/
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        
        if segue.identifier == "Preview"{
            let controller = segue.destination as! PreviewController
            controller.imageURLString = selectedURL
            controller.imageSelected = selectedImage
        }
        
     }
 
    
    @IBAction func maskAction(_ sender: Any) {
        
    }
    @IBAction func sendMessageAction(_ sender: Any) {
        var FID : NSNumber = 1
        if isAdmin == false{
            FID = selectedUser.uid
        }
        
        
        let str : String = textInputBar.text.Trim()
        if str.characters.count > 0{
        let urlString : String = URLConstants.baseUrl + URLConstants.chatMessageApi
        let fileData : UIImage? = nil
        //fileData = UIImage(named : "placeholder")!
        let textString : String = textInputBar.text
        let param : [String : String] = ["From_Id":FID.stringValue,"To_Id":userID.stringValue,"Text":textString]
        var fromString : String = ""
        var toString : String = ""
        if self.chatArray.count > 0{
            let chat : ChatBox = self.chatArray[0]
            
            fromString = chat.friendImage
            toString = chat.userImage
        }
        if fileData == nil{
            let chat : NSDictionary = ["From_Id":FID,"To_Id":userID,"Text":textString,"img":"","Date":chatDateString,"ToImage":toString,"FromImage":fromString]
            let box : ChatBox = ChatBox.init(chat: chat)
            self.chatArray.append(box)
             let oldLastCellIndexPath = IndexPath(row: self.chatArray.count-1, section: 0)
            self.chatTableView.insertRows(at: [oldLastCellIndexPath], with: .left)
            self.chatTableView.scrollToRow(at: oldLastCellIndexPath, at: .bottom, animated: false)
        }
        textInputBar.text = ""
        ServerData.uploadTaskFor(URLString: urlString, parameters: param  , file: nil, showIndicator: false) { (dataDictionary) in
            print(dataDictionary)
        }
        }else{
            FTIndicator.showToastMessage("Can't Send Empty Message")
        }
    }
    
    
    
    func getChat(){
        var param  : [String : String] = [:]
        if isAdmin == true{
             param  = ["To_Id" : userID.stringValue , "From_Id" : "1"]
        }else{
            param  = ["To_Id" : userID.stringValue , "From_Id" : selectedUser.uid.stringValue]
        }
        
       
        let urlString = URLConstants.baseUrl + URLConstants.getChatMessages + param.convertDictionary()
        
        
        ServerData.getResponseFromURL(urlString: urlString, loaderNeed: true) { (dataDictionary) in
            print(dataDictionary)
            if let chats = dataDictionary["Chats"] as? NSArray{
                self.mask = dataDictionary["Mask"] as! NSNumber
                for i in 0 ..< chats.count {
                    let dic = chats[i] as! NSDictionary
                    let chat : ChatBox = ChatBox.init(chat: dic)
                    self.chatArray.append(chat)
                }
                print(self.chatArray)
                self.chatTableView.reloadData()
                let oldLastCellIndexPath = IndexPath(row: self.chatArray.count-1, section: 0)
                self.chatTableView.scrollToRow(at: oldLastCellIndexPath, at: .bottom, animated: false)
            }
           
            
            
        }
    }
    
    @IBAction func backAction(_ sender: Any) {//Card Back Action
        self.navigationController?.popViewController(animated: true)
    }
}

extension ChatController : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let oldLastCellIndexPath = IndexPath(row: self.chatArray.count-1, section: 0)
        self.chatTableView.scrollToRow(at: oldLastCellIndexPath, at: .bottom, animated: false)
    }
}

extension ChatController : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let chat : ChatBox = chatArray[indexPath.row]
        
        
        
        if chat.isFriend == true{
            if chat.isImage == true{
                let cell:FriendImageCell = tableView.dequeueReusableCell(withIdentifier: "FriendImageCell", for: indexPath) as! FriendImageCell
                cell.backgroundColor = UIColor.clear
                let urlString : String = chat.img!
                if let imageURL : URL = URL(string : urlString){
                    cell.msgImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "backImage"),options: SDWebImageOptions.retryFailed, completed: nil)
                }
                if  self.mask == 0{
                    cell.friendImageView.image = UIImage(named : "mask")!
                }else{
                    let furlString : String = chat.friendImage
                    if let imageURL : URL = URL(string : furlString){
                        cell.friendImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder"),options: SDWebImageOptions.retryFailed, completed: nil)
                    }
                }
                
                cell.selectionStyle = .none

                return cell
            }else{
                let cell:FriendTextCell = tableView.dequeueReusableCell(withIdentifier: "FriendTextCell", for: indexPath) as! FriendTextCell
                cell.backgroundColor = UIColor.clear
                if  self.mask == 0{
                    cell.friendImageView.image = UIImage(named : "mask")!
                }else{
                    let urlString : String = chat.friendImage
                    if let imageURL : URL = URL(string : urlString){
                        cell.friendImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder"),options: SDWebImageOptions.retryFailed, completed: nil)
                    }
                }
                cell.msgLabel.text = chat.msg
                cell.selectionStyle = .none

                return cell
            }
        }else{
            if chat.isImage == true{
                let cell:UserImageCell = tableView.dequeueReusableCell(withIdentifier: "UserImageCell", for: indexPath) as! UserImageCell
                cell.backgroundColor = UIColor.clear
                
                if chat.recentImage !=  nil{
                    cell.msgImageView.image = chat.recentImage
                }else{
                    let urlString : String = chat.img!
                    if let imageURL : URL = URL(string : urlString){
                        cell.msgImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "backImage"),options: SDWebImageOptions.retryFailed, completed: nil)
                    }
                }
         
                let myurlString : String = chat.userImage
                if let imageURL : URL = URL(string : myurlString){
                    cell.userImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder"),options: SDWebImageOptions.retryFailed, completed: nil)
                }
                
                cell.selectionStyle = .none

                return cell
            }else{
                let cell:UserTextCell = tableView.dequeueReusableCell(withIdentifier: "UserTextCell", for: indexPath) as! UserTextCell
                cell.backgroundColor = UIColor.clear
                cell.msgLabel.text = chat.msg
                
                let myurlString : String = chat.userImage
                if let imageURL : URL = URL(string : myurlString){
                    cell.userImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder"),options: SDWebImageOptions.retryFailed, completed: { (img, error, cacheType, imageURL) in
                    })
                }
                cell.selectionStyle = .none
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
          let chat : ChatBox = chatArray[indexPath.row]
        if chat.isImage{
            self.selectedURL = chat.img
            self.performSegue(withIdentifier: "Preview", sender: self)
        }
        if chat.recentImage !=  nil{
            self.selectedImage = chat.recentImage
            self.performSegue(withIdentifier: "Preview", sender: self)
        }
    }
}
extension ChatController :  UIImagePickerControllerDelegate , UINavigationControllerDelegate ,UIPopoverControllerDelegate{
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        picker.dismiss(animated: true, completion: nil)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var imageSelected = info[UIImagePickerControllerOriginalImage] as? UIImage
        imageSelected = imageSelected?.resizeImageWith(newSize: CGSize(width : self.view.frame.size.width, height :self.view.frame.size.height))
        picker.dismiss(animated: true) {
            let urlString : String = URLConstants.baseUrl + URLConstants.chatMessageApi
            let fileData : UIImage? = imageSelected
            let textString : String = ""
            let param : [String : String] = ["From_Id":self.selectedUser.uid.stringValue ,"To_Id":self.userID.stringValue,"Text":textString]
            var fromString : String = ""
            var toString : String = ""
            if self.chatArray.count > 0{
                let chat : ChatBox = self.chatArray[0]
                fromString = chat.friendImage
                toString = chat.userImage
            }
  
            let chat : NSDictionary = ["From_Id":self.selectedUser.uid,"To_Id":self.userID,"Text":textString,"img":fileData!,"Date":self.chatDateString,"ToImage":toString,"FromImage":fromString]
            let box : ChatBox = ChatBox.init(chat: chat)
            self.chatArray.append(box)

            
            let oldLastCellIndexPath = IndexPath(row: self.chatArray.count-1, section: 0)
            self.chatTableView.insertRows(at: [oldLastCellIndexPath], with: .left)
            self.chatTableView.scrollToRow(at: oldLastCellIndexPath, at: .bottom, animated: false)
            
            
            ServerData.uploadTaskFor(URLString: urlString, parameters: param  , file: fileData, showIndicator: false) { (dataDictionary) in
                print(dataDictionary)
            }
            
        }
        
        
    }
    
    
    
    
}
extension Foundation.Date {
    func dashedStringFromDate() -> String {
        let dateFormatter = DateFormatter()
        let date = self
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")

        return dateFormatter.string(from: date)
    }
}
