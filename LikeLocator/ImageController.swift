//
//  ImageController.swift
//  LikeLocator
//
//  Created by ok on 9/20/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit

class ImageController : UIViewController {
    var picker:UIImagePickerController?=UIImagePickerController()
    var userDictionary : Dictionary<String, Any>!
    var imageSelected : UIImage!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(userDictionary)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    func registerUser(){
        
    }
    
    @IBAction func buttonAction(_ sender : UIButton){
        
        switch sender.tag {
        case 10:
            self.openGallary()
            break;
        case 20:
            self.openCamera()
            break;
        default:
            break;
        }
    }
    
    func openGallary()
    {
        picker!.allowsEditing = false
        picker?.delegate = self
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(picker!, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            picker!.allowsEditing = false
            picker?.delegate = self
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            picker!.cameraCaptureMode = .photo
            present(picker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "upload"{
            let nextScene = segue.destination as! RegistrationController
            nextScene.userDictionary = userDictionary
            nextScene.selectedImage = imageSelected
        }
    }
    
    
}
extension ImageController :  UIImagePickerControllerDelegate , UINavigationControllerDelegate ,UIPopoverControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        picker.dismiss(animated: true, completion: nil)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imageSelected = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        picker.dismiss(animated: true) {
           self.performSegue(withIdentifier: "upload", sender: self)
        }
    }
}
