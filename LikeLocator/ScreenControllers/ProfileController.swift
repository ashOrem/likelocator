//
//  ProfileController.swift
//  LikeLocator
//
//  Created by ok on 9/19/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit
import SDWebImage
import FTIndicator

class ProfileController: UIViewController {
    var picker:UIImagePickerController?=UIImagePickerController()
    var imageSelected : UIImage!
    var uname : String!
    var upass : String!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var userNameTxtField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordTxtField.setBottomBorder()
        userNameTxtField.setBottomBorder()
        
        
        if let user = UserDefaults.standard.object(forKey: "User") as? NSDictionary{
            if let imageURL = user["ImageUri"] as? String{
                if let url = URL(string : imageURL){
                    self.profileImage.sd_setImage(with:  url, placeholderImage: UIImage(named : "placeholder"), options: SDWebImageOptions.avoidAutoSetImage, completed: { (img, err, cache, urlString) in
                        if img != nil{
                            let maskedImage : UIImage = (img?.circleMasked)!
                            self.profileImage.image = maskedImage
                        }
                    })
                    self.getProfile(user: user, serverCall: false)
                }else{
                     self.getProfile(user: user, serverCall: true)
                }
            }else{
                self.getProfile(user: user, serverCall: true)
            }
        }
        
       
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func adminChatAction(_ sender: Any) {
        self.performSegue(withIdentifier: "Chat", sender: self)
    }
    @IBAction func backAction(_ sender: Any) {//Card Back Action
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func logoutAction(_ sender: Any) {//Card Back Action
        let alertController = UIAlertController(title: "Are you sure you want to", message: "Logout?", preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Later", style: .cancel) { action in
        }
        alertController.addAction(cancelAction)
        let OKAction = UIAlertAction(title: "Logout", style: .default) { action in
            UserDefaults.standard.removeObject(forKey: "Login")
            UserDefaults.standard.synchronize()
            self.performSegue(withIdentifier: "Register", sender: self)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true) {
        }
        
    }
    @IBAction func updateAction(_ sender: Any) {//Card Back Action
        var unchangedName : Bool = false
        var unchangedPassword : Bool = false
        if userNameTxtField.text == uname{
            unchangedName = true
        }
        if passwordTxtField.text == upass{
                unchangedPassword = true
        }
        if unchangedPassword == true && unchangedName == true{
               FTIndicator.showToastMessage("Updated Successfully")
        }else{
            self.updateProfile()
        }
    }
    
    func updateProfile(){
        if let user = UserDefaults.standard.object(forKey: "User") as? NSDictionary{
            let mutDict : NSMutableDictionary = NSMutableDictionary(dictionary : user)
            mutDict.setObject(userNameTxtField.text!, forKey: "username" as NSCopying)
            mutDict.setObject(passwordTxtField.text!, forKey: "password" as NSCopying)
            var chkStr : String = "false"

            if let gender : Bool = user["gender"] as? Bool{
                if gender == true{
                    chkStr = "true"
                }
            }
           
           let uid : NSNumber = user["User_Id"] as! NSNumber
            let param : [String : String] = ["Email" : userNameTxtField.text! , "Password" : passwordTxtField.text! , "id" : uid.stringValue ,"Sex" : chkStr]
            
            let urlString : String = URLConstants.baseUrl + URLConstants.registerUpdateApi
            ServerData.postResponseFromURL(urlString: urlString, showIndicator: true, params: param) { (dataDictionary) in
                print(dataDictionary)
                    UserDefaults.standard.set(mutDict, forKey: "User")
                    UserDefaults.standard.synchronize()
                FTIndicator.showToastMessage("Updated Successfully")
            }
            
        }
        
        

 
    }
    
    @IBAction func editImageAction(_ sender: Any) {
        let alertController = UIAlertController(title: "Select Image From :", message: "", preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
        }
        alertController.addAction(cancelAction)
        let camAction = UIAlertAction(title: "Camera", style: .default) { action in
            self.openCamera()
        }
        alertController.addAction(camAction)
        
        let gallaryAction = UIAlertAction(title: "Gallary", style: .default) { action in
            self.openGallary()
        }
        alertController.addAction(gallaryAction)
        self.present(alertController, animated: true) {
        }
        
 
    }
    
    func openGallary()
    {
        picker!.allowsEditing = false
        picker?.delegate = self
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(picker!, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            picker!.allowsEditing = false
            picker?.delegate = self
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            picker!.cameraCaptureMode = .photo
            present(picker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        if segue.identifier == "Chat"{
            let controller = segue.destination as! ChatController
            controller.selectedUser = nil
        }
        if segue.identifier == "ProfilePreview"{
            let controller = segue.destination as! ProfilePreviewController
            if let user = UserDefaults.standard.object(forKey: "User") as? NSDictionary{
                controller.selectedImage = imageSelected
                if let gender = user["gender"] as? Bool{
                    controller.maleFemale = gender
                }
            }
            
        }
     }
 
    
    
    
    func getProfile(user : NSDictionary, serverCall : Bool){
            let mutDictionary : NSMutableDictionary = NSMutableDictionary(dictionary : user)
            uname  = user["username"] as! String
            upass  = user["password"] as! String
            userNameTxtField.text = uname
            passwordTxtField.text = upass
        if serverCall == true{
            let uid : NSNumber = user["User_Id"] as! NSNumber
            let param : [String : String] = ["To_Id" : uid.stringValue]
            let urlString : String = URLConstants.baseUrl + URLConstants.getProfielApi + param.convertDictionary()
            ServerData.getResponseFromURL(urlString: urlString, loaderNeed: true, completion: { (dataDictionary) in
                print(dataDictionary)
                if let status = dataDictionary["Status"] as? NSNumber{
                    if status == 1{
                        let dict  : NSDictionary = dataDictionary["result"] as! NSDictionary
                        var numberOFLikes : NSNumber = 0
                        
                        
                        let sex = dict["Sex"] as! Bool
                        
                        if let likes = dict["Likes"] as? NSNumber{
                            self.likeLabel.text = "Likes : \(likes)"
                            numberOFLikes = likes
                        }
                        var userImage : String = ""
                        if let userImg = dict["ImageUri"] as? String{
                            userImage = userImg
                            let urlString : URL =  URL(string : userImg)!
                            self.profileImage.sd_setImage(with:  urlString, placeholderImage: UIImage(named : "placeholder"), options: SDWebImageOptions.avoidAutoSetImage, completed: { (img, err, cache, urlString) in
                                if img != nil{
                                    let maskedImage : UIImage = (img?.circleMasked)!
                                    self.profileImage.image = maskedImage
                                }
                            })
                        }
                        mutDictionary.setValue(numberOFLikes, forKey: "likes")
                        mutDictionary.setValue(userImage, forKey: "ImageUri")
                        mutDictionary.setValue(sex, forKey: "gender")
                        
                        UserDefaults.standard.set(mutDictionary, forKey: "User")
                        UserDefaults.standard.synchronize()
                        
                        //                        ////Saved UserDetail Globally
                        //                        let shared : GlobalME = GlobalME.sharedGlobal
                        //                        let me : MySelf = MySelf.init(name: shared.member.name, imageUrl: userImage, uid: shared.member.uid, password: shared.member.password, gender: sex,likes :numberOFLikes)
                        //                        shared.member = me
                        //                        /////////////////////////
                        
                        
                    }
                }
            })
        }

        
    }
}

extension ProfileController :  UIImagePickerControllerDelegate , UINavigationControllerDelegate ,UIPopoverControllerDelegate{
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        picker.dismiss(animated: true, completion: nil)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imageSelected = info[UIImagePickerControllerOriginalImage] as? UIImage
        picker.dismiss(animated: true) {
                   self.performSegue(withIdentifier: "ProfilePreview", sender: self)
        }
    }
    
    
    
    
}
