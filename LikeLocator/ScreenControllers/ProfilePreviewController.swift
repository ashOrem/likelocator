//
//  ProfilePreviewController.swift
//  LikeLocator
//
//  Created by MAC on 26/09/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit
import SDWebImage
import FTIndicator
class ProfilePreviewController: UIViewController {

    var selectedImage : UIImage?
    var maleFemale : Bool?
    @IBOutlet weak var profileImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if selectedImage != nil{
            let img = self.selectedImage?.circleMasked
            selectedImage = selectedImage?.resizeImageWith(newSize: CGSize(width : self.view.frame.size.width, height : self.view.frame.size.height))
            self.profileImageView.image = img
        }
        // Do any additional setup after loading the view.
    }
    
    func uploadProfileImage(){
        if let user = UserDefaults.standard.object(forKey: "User") as? NSDictionary{
            if let uid = user["User_Id"] as? NSNumber{
        FTIndicator.showProgress(withMessage: "Updating")
        let urlString : String = URLConstants.baseUrl + URLConstants.userImageUploadApi
                ServerData.uploadProfileImage(URLString: urlString, header: uid.stringValue, file: selectedImage!) { (dataDictionary) in
            FTIndicator.dismissProgress()
            FTIndicator.showToastMessage("Updated Successfully")
            self.dismiss(animated: true, completion: nil)
        }
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func femaleAction(_ sender: Any) {
        self.uploadProfileImage()
    }
    @IBAction func maleAction(_ sender: Any) {
          self.uploadProfileImage()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

