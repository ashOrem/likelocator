//
//  RegistrationController.swift
//  LikeLocator
//
//  Created by ok on 9/20/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit
import FTIndicator
class RegistrationController: UIViewController {
    
    var userDictionary : Dictionary<String, Any>!
    var selectedImage : UIImage!
    @IBOutlet weak var userImageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        let img : UIImage = selectedImage
        userImageView.image  = img.circleMasked
        
        selectedImage = selectedImage.resizeImageWith(newSize: CGSize(width : self.view.frame.size.width , height : self.view.frame.size.height))
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        userImageView.layer.cornerRadius = userImageView.frame.size.width / 2
        userImageView.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   @IBAction func backAction(_ sender : UIButton){
    self.dismiss(animated: true, completion: nil)
    }
    @IBAction func maleFemaleAction(_ sender : UIButton){
        switch sender.tag {
        case 10:
            self.registerUser(selectedGender: "true")
            break;
        case 20:
            self.registerUser(selectedGender: "false")

            break;
        default:
            break;
        }
        
    }
    
    func registerUser(selectedGender : String){
        let param : [String : String] = ["Email" : userDictionary["username"] as! String , "Password" : userDictionary["password"] as! String , "id" : "" ,"Sex" : selectedGender]
        let urlString : String = URLConstants.baseUrl + URLConstants.registerUpdateApi
        ServerData.postResponseFromURL(urlString: urlString, showIndicator: false, params: param) { (dataDictionary) in
            print(dataDictionary)
            if let uid = dataDictionary["UserId"] as? NSNumber{
                self.userDictionary["User_Id"] = uid
                if selectedGender == "true"{
                     self.userDictionary["gender"] = true
                }else{
                     self.userDictionary["gender"] = false
                }
        
                UserDefaults.standard.set(self.userDictionary, forKey: "User")
                UserDefaults.standard.synchronize()
                UserDefaults.standard.set(true, forKey: "Login")
                UserDefaults.standard.synchronize()
                self.uploadProfileImage()
            }
        }
    }
    
    func uploadProfileImage(){
        FTIndicator.showProgress(withMessage: "Signing Up")
        let uid : String = (userDictionary["User_Id"] as! NSNumber).stringValue
        print(selectedImage)
        let urlString : String = URLConstants.baseUrl + URLConstants.userImageUploadApi
        ServerData.uploadProfileImage(URLString: urlString, header: uid, file: selectedImage) { (dataDictionary) in
            FTIndicator.dismissProgress()
            FTIndicator.showToastMessage("Successfully Registered")
            self.performSegue(withIdentifier: "Main", sender: self)
        }
        
    }
    
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
 

}
extension UIImage {
    var isPortrait:  Bool    { return size.height > size.width }
    var isLandscape: Bool    { return size.width > size.height }
    var breadth:     CGFloat { return min(size.width, size.height) }
    var breadthSize: CGSize  { return CGSize(width: breadth, height: breadth) }
    var breadthRect: CGRect  { return CGRect(origin: .zero, size: breadthSize) }
    var circleMasked: UIImage? {
        UIGraphicsBeginImageContextWithOptions(breadthSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        guard let cgImage = cgImage?.cropping(to: CGRect(origin: CGPoint(x: isLandscape ? floor((size.width - size.height) / 2) : 0, y: isPortrait  ? floor((size.height - size.width) / 2) : 0), size: breadthSize)) else { return nil }
        UIBezierPath(ovalIn: breadthRect).addClip()
        UIImage(cgImage: cgImage, scale: 1, orientation: imageOrientation).draw(in: breadthRect)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
