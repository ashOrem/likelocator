//
//  HomeController.swift
//  LikeLocator
//
//  Created by ok on 9/19/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit
import FTIndicator
import BFPaperButton
import SDWebImage

class HomeController: UIViewController {
    // MARK: - Variabes & Outlets
    @IBOutlet weak var userCollector: UICollectionView!
    @IBOutlet weak var nextBtn: BFPaperButton!
    @IBOutlet weak var blocktBtn: BFPaperButton!
    @IBOutlet weak var chatBtn: BFPaperButton!
    @IBOutlet weak var likeBtn: BFPaperButton!
    @IBOutlet weak var likeImage: UIImageView!
    @IBOutlet weak var distanceImage: UILabel!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    var currentIndex : Int = 0
    var viewHeight : CGFloat = 0
    var viewWidth : CGFloat = 0
    var definedSize : CGSize!
    var usersArray : [SecondUser] = []
    var userID : NSNumber!
    // MARK: - View_Life_Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let app : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        viewWidth = (app.window?.bounds.size.width)!
        viewHeight  = (app.window?.bounds.size.height)!
        definedSize = CGSize(width : viewWidth, height : viewHeight)
        self.customizeButtons(btn: blocktBtn)
        self.customizeButtons(btn: nextBtn)
        self.customizeButtons(btn: chatBtn)
        self.customizeButtons(btn: likeBtn)
        
        if let user = UserDefaults.standard.object(forKey: "User") as? NSDictionary{
            let uid : NSNumber = user["User_Id"] as! NSNumber
            userID = uid
        }
        
        self.getUsers()
        
        nameLabel.text = ""
        distanceLabel.text = ""
        likeLabel.text = ""
        likeImage.alpha = 0
        distanceImage.alpha = 0
        nameLabel.layer.shadowOpacity = 0.8
        nameLabel.layer.shadowOffset = CGSize(width : 0, height : 0)
        distanceLabel.layer.shadowOpacity = 0.8
        distanceLabel.layer.shadowOffset = CGSize(width : 0, height : 0)
        likeLabel.layer.shadowOpacity = 0.8
        likeLabel.layer.shadowOffset = CGSize(width : 0, height : 0)
        
        self.backBtn.alpha = 0.4
        self.backBtn.isUserInteractionEnabled = false
        // Do any additional setup after loading the view.
    }
    

    
    override var prefersStatusBarHidden: Bool{
        return false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Actions_For_the_Controller
    @IBAction func backAction(_ sender: Any) {//Card Back Action
        if currentIndex > 0{
            currentIndex = currentIndex - 1
            userCollector.setContentOffset(CGPoint(x:  CGFloat(currentIndex) * viewWidth, y: 0), animated: true)
        }
         self.showDetails()
        self.checkButton_HideandShow()
    }
    @IBAction func chatAction(_ sender: Any) {
        self.performSegue(withIdentifier: "Chat", sender: self)
    }
    @IBAction func nextAction(_ sender: Any) {//Card Next Action
        if currentIndex  < usersArray.count - 1 {
            currentIndex = currentIndex + 1
            userCollector.setContentOffset(CGPoint(x:  CGFloat(currentIndex) * viewWidth, y: 0), animated: true)
        }else{
            currentIndex = 0
            userCollector.setContentOffset(CGPoint(x:  CGFloat(currentIndex) * viewWidth, y: 0), animated: false)
        }
        self.showDetails()
        self.checkButton_HideandShow()
    }
    @IBAction func reportUser(_ sender: Any){//Report a User
        let alertController = UIAlertController(title: "Report", message: "Are you sure you want to report \(nameLabel.text!)", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
        }
        alertController.addAction(cancelAction)
        let OKAction = UIAlertAction(title: "Report", style: .default) { action in
            self.reportFunc()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true) {
        }
    }
    @IBAction func blockAction(_ sender: Any) {//Block User
        let urlString : String = URLConstants.baseUrl + URLConstants.blockApi
        let param : [String : Any] = ["From_Id":"523","To_Id":userID.stringValue]
        self.likeBlock(isBlocked: true, url: urlString, param: param as! [String : String])
    }
    @IBAction func likeAction(_ sender: Any) {//Like User
        let urlString : String = URLConstants.baseUrl + URLConstants.likeApi
        let param : [String : Any] = ["From_Id":"523","To_Id":userID.stringValue]
        self.likeBlock(isBlocked: false, url: urlString, param: param as! [String : String])
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Chat"{
            let controller = segue.destination as! ChatController
             let structDic : SecondUser = usersArray[currentIndex]
            let dict : NSDictionary = ["User_Id":structDic.uid,"UserName":structDic.name,"Image":structDic.imageUrl,"Started_Id":0,"Mask":0,"ChatStatus":0]
            let chatBox : InboxUser = InboxUser.init(user: dict)
            controller.selectedUser = chatBox
        }
        
    }
    
    // MARK: - Private Class Methods
    func reportFunc(){//Report API Call
        let param : [String : Any] = ["From_Id":"948","To_Id":userID.stringValue]
        let urlString : String = URLConstants.baseUrl + URLConstants.reportApi + param.convertDictionary()
        ServerData.getResponseFromURL(urlString: urlString, loaderNeed: false) { (dataDictionary) in
            print(dataDictionary)
            FTIndicator.showToastMessage("\(self.nameLabel.text!) Reported Successfully")
        }
    }
    func likeBlock(isBlocked : Bool , url : String , param : [String : String]){//Like & Block API Call
        print(param.convertDictionary())
        let apiUrl  = url + param.convertDictionary()
        ServerData.getResponseFromURL(urlString: apiUrl, loaderNeed: true) { (dataDictionary) in
            print(dataDictionary)
            
            if let status = dataDictionary["Status"] as? NSNumber{
                if status == 0{
                    if let msg = dataDictionary["Message"] as? String{
                        FTIndicator.showToastMessage(msg)
                    }
                }else{
                    if isBlocked == false{
                        var user : SecondUser = self.usersArray[self.currentIndex]
                        var num : NSNumber =  user.likes
                        num = NSNumber(value: num.intValue + 1)
                        user.likes = num
                        self.usersArray[self.currentIndex] = user
                        self.likeLabel.text = "\(num)"
                        
                          FTIndicator.showToastMessage("\(self.nameLabel.text!)  Successfully Liked")
                    }else{
                         FTIndicator.showToastMessage("\(self.nameLabel.text!) Successfully Blocked")
                     
                    }
                }
              }
            
        
        }
    }
    func checkButton_HideandShow(){//Hide & Show Back Button
        if currentIndex == 0{
            self.backBtn.alpha = 0.4
            self.backBtn.isUserInteractionEnabled = false
        }
        else {
            self.backBtn.alpha = 1.0
            self.backBtn.isUserInteractionEnabled = true
        }
    }
    func showDetails(){//Show Details Of User on Screen
        if self.usersArray.count > 0{
            let person : SecondUser = self.usersArray[currentIndex]
            let like : NSNumber = person.likes
            self.likeLabel.text = like.stringValue
            self.nameLabel.text = person.name
            let dist : NSNumber = person.dic
            let distance : Double  = dist.doubleValue * 1000.0
            let distString : String = String(format: "%.2f Km", distance)
            self.distanceLabel.text = distString
        }
    }
    func customizeButtons(btn : BFPaperButton){//Customized Bottom Buttons
        btn.tapCircleColor = UIColor(red: 13/255.0, green: 185/255.0, blue: 136/255.0, alpha: 0.3)
        btn.cornerRadius = btn.frame.size.width / 2.0
        btn.rippleFromTapLocation = false
        btn.rippleBeyondBounds = true
        btn.tapCircleDiameter = max(btn.frame.size.width,  btn.frame.size.height) * 0.2;
    }
    func getUsers(){//Get All the Users from server
        if let lat = UserDefaults.standard.object(forKey: "Lat") as? CGFloat{
            if let lng = UserDefaults.standard.object(forKey: "Lng") as? CGFloat{
                
                let param : [String : Any] = ["User_Id":userID.stringValue,"Lat":lat,"Lng":lng]
                let urlString : String = URLConstants.baseUrl + URLConstants.userLoadApi + param.convertDictionary()
                print(urlString)
                ServerData.getResponseFromURL(urlString: urlString, loaderNeed: true) { (dataDictionary) in
                    if let status = dataDictionary["Status"] as? Bool{
                        if status == true{
                            if let users = dataDictionary["users"] as? NSArray{
                                for i in 0 ..< users.count {
                                    let dict : NSDictionary = users[i] as! NSDictionary
                                    let user : SecondUser = SecondUser.init(card: dict)
                                    self.usersArray.append(user)
                                }
                                self.showDetails()
                                self.likeImage.alpha = 1
                                self.distanceImage.alpha = 1
                                self.userCollector.reloadData()
                                self.updateToken()
                            
                            }
                        }else{
                            FTIndicator.showToastMessage("Users Not Found")
                        }
                    }
                }
            }
        }
        
        
    }
    func updateLocation(){
        if let lat = UserDefaults.standard.object(forKey: "Lat"){
            if let lng = UserDefaults.standard.object(forKey: "Lng"){
                let param : [String : Any] = ["User_Id":userID.stringValue,"lat":"\(lat)","lng":"\(lng)"]
                let urlString : String = URLConstants.baseUrl + URLConstants.locationUpdate + param.convertDictionary()
                ServerData.getResponseFromURL(urlString: urlString, loaderNeed: false, completion: { (dataDictionary) in
                    print(dataDictionary)
                })
                
            }
        }
    }
    
    func updateToken(){//Update a token when user enters
        if let token = UserDefaults.standard.object(forKey: "DToken") as? String{
            let param : [String : Any] = ["User_Id":userID,"Token":token,"Type":"2"]
            let urlString : String = URLConstants.baseUrl + URLConstants.deviceApi + param.convertDictionary()
            ServerData.getResponseFromURL(urlString: urlString, loaderNeed: false) { (dataDictionary) in
                self.updateLocation()
            }
        }
    }
}

extension HomeController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
 
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return usersArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Image", for: indexPath as IndexPath)
        
        let structDic : SecondUser = usersArray[indexPath.row]
        let imgView : UIImageView = cell.viewWithTag(10) as! UIImageView
        let urlString : String = structDic.imageUrl
        let imageURL : URL = URL(string : urlString)!
        
        imgView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "backImage"),options: SDWebImageOptions.highPriority, completed: { (img, error, cacheType, imageURL) in
            imgView.image = img?.resizeImageWith(newSize: self.definedSize)
            imgView.contentMode = .scaleAspectFill
        })
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width : viewWidth ,height: viewHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
   
    
}

