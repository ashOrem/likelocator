//
//  LoginController.swift
//  LikeLocator
//
//  Created by ok on 9/19/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit
import FTIndicator

class LoginController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var userNameTxtField: UITextField!
    var param : [String : Any]!
    
    // MARK: - View_Life_Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordTxtField.setBottomBorder()
        userNameTxtField.setBottomBorder()
        

        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let user = UserDefaults.standard.object(forKey: "User") as? NSDictionary{
            let uname : String = user["username"] as! String
            let upass : String = user["password"] as! String
            userNameTxtField.text = uname
            passwordTxtField.text = upass
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Image"{
            let nextScene = segue.destination as! ImageController
            nextScene.userDictionary = param
        }
    }
    
    
    // MARK: - Actions & Methods
    func singUserIn(user : String , pass : String){
        param = ["password":pass,"username":user]
        let urlString : String = URLConstants.baseUrl + URLConstants.loginApi
        ServerData.postResponseFromURL(urlString: urlString, showIndicator: true, params: param as! [String : String]) { (dataDictionary) in
            print(dataDictionary)
            let checkRegistration : NSNumber = dataDictionary["IsRegistered"] as! NSNumber
            if checkRegistration == 0{
                self.performSegue(withIdentifier: "Image", sender: self)
            }else{
                let uid : NSNumber = dataDictionary["UserId"] as! NSNumber
                self.param["User_Id"] = uid
                
//                ////Saved UserDetail Globally
//                let me : MySelf = MySelf.init(name: user, imageUrl: "", uid: uid, password: pass, gender: false,likes :0)
//                let shared : GlobalME = GlobalME.sharedGlobal
//                shared.member = me
//                /////////////////////////
                
                print(self.param)
                UserDefaults.standard.set(self.param, forKey: "User")
                UserDefaults.standard.synchronize()
                UserDefaults.standard.set(true, forKey: "Login")
                UserDefaults.standard.synchronize()
                self.performSegue(withIdentifier: "Login", sender: self)
            }
        }
    }
    
    @IBAction func loginAction(_ sender : UIButton){
        self.view.endEditing(true)
        let password : String = passwordTxtField.text!.Trim()
        let username : String = userNameTxtField.text!.Trim()
        if password.characters.count == 0 || username.characters.count == 0{
            FTIndicator.showToastMessage("Please Enter Your Credentials")
            return
        }
        if (passwordTxtField.text?.characters.count)! < 6{
            FTIndicator.showToastMessage("Password must have atleast 6 characters")
            return
        }
        
        self.singUserIn(user: username, pass: password)
    }
}
