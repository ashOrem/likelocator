//
//  InboxController.swift
//  LikeLocator
//
//  Created by MAC on 21/09/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit
import FTIndicator
import SDWebImage

class InboxController: UIViewController {
    @IBOutlet weak var inboxTableView: UITableView!
    var inboxArray : [InboxUser] = []
    var helloWorldTimer : Timer!
    var selectedData : InboxUser!
    var userID : NSNumber!
    override func viewDidLoad() {
        super.viewDidLoad()
        
         helloWorldTimer = Timer.scheduledTimer(timeInterval: 600.0, target: self, selector: #selector(InboxController.deleteChat), userInfo: nil, repeats: true)
        

        
        
        if let user = UserDefaults.standard.object(forKey: "User") as? NSDictionary{
            let uid : NSNumber = user["User_Id"] as! NSNumber
            userID = uid
        }
        inboxTableView.tableFooterView = UIView()
        self.getInbox()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
     @IBAction func backAction(_ sender: Any) {//Card Back Action
        self.navigationController?.popViewController(animated: true)
    }
    
    func deleteChat()
    {
        NSLog("hello Delete")
    }
    
    func getInbox(){
        let param : [String : String] = ["To_Id":userID.stringValue]
        let urlString : String = URLConstants.baseUrl + URLConstants.inboxApi + param.convertDictionary()
        ServerData.getResponseFromURL(urlString: urlString, loaderNeed: true) { (dataDictionary) in
            print(dataDictionary)
            if let status = dataDictionary["Status"] as? Bool{
                if status == true{
                    if let users = dataDictionary["Chats"] as? NSArray{
                        for i in 0 ..< users.count {
                            let dict : NSDictionary = users[i] as! NSDictionary
                            let user : InboxUser = InboxUser.init(user: dict)
                            self.inboxArray.append(user)
                        }
                        self.inboxTableView.reloadData()
                    }
                }else{
                    FTIndicator.showToastMessage("Inbox is Empty")
                }
            }
        }
    }
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Chat"{
            let controller = segue.destination as! ChatController
            controller.selectedUser = selectedData
        }
     }

    
}

extension InboxController : UITableViewDataSource , UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return inboxArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InboxCell", for: indexPath) as! InboxCell
        cell.selectionStyle = .none
        let inboxData : InboxUser = self.inboxArray[indexPath.row]
        let imgUrl : String = inboxData.imageUrl
        let URLis : URL = URL(string : imgUrl)!
        
        let mask : NSNumber = inboxData.mask
         let stID : NSNumber = inboxData.startId
        
        if mask == 0 && stID == userID{
            cell.userImageView.sd_setImage(with:  URLis, placeholderImage: UIImage(named : "placeholder"), options: SDWebImageOptions.avoidAutoSetImage, completed: { (img, err, cache, urlString) in
                if img != nil{
                    let maskedImage : UIImage = (img?.circleMasked)!
                    cell.userImageView.image = maskedImage
                }
            })
        }else if  stID != userID{
            cell.userImageView.image = UIImage(named  : "mask")
        }
 
        if inboxData.status == 0{
            cell.seenLabel.isHidden = false
        }else{
            cell.seenLabel.isHidden = true
        }

        cell.nameLabel.text = inboxData.name
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 94
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
         selectedData = self.inboxArray[indexPath.row]
        
        self.performSegue(withIdentifier: "Chat", sender: self)
        
    }
}

