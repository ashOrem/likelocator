//
//  ServerData.swift
//  TextFieldValidations
//
//  Created by ok on 9/14/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit
import Alamofire
import FTIndicator
import Foundation
class ServerData: NSObject {

    // MARK: GETFROMSERVER
    class func getResponseFromURL(urlString: String,loaderNeed: Bool,  completion: @escaping (_ success: [String : AnyObject]) -> Void){
        if loaderNeed{
                FTIndicator.showProgress(withMessage: DefinedStrings.loaderText)
            }
            Alamofire.request(urlString).responseJSON { response in
                if loaderNeed{
                    FTIndicator.dismissProgress()
                }
                switch response.result {
                case .success:
                    let json = response.result.value
                    completion(json as! [String : AnyObject])
                case .failure(let error):
                    FTIndicator.showToastMessage("Server not responding")
                
                    
                }
            }
        }
    
    
    // MARK: POSTTOSERVER
    class func postResponseFromURL(urlString: String, showIndicator : Bool, params : [String : String],  completion: @escaping (_ success: [String : AnyObject]) -> Void){
        
            if showIndicator == true{
                FTIndicator.showProgress(withMessage: DefinedStrings.loaderText)
            }
            Alamofire.request(urlString, method: .post, parameters: params, encoding: JSONEncoding.default, headers: [:]).responseJSON { (response:DataResponse<Any>) in
                if showIndicator == true{
                    FTIndicator.dismissProgress()
                }
                switch(response.result) {
                case .success(_):
                        let json = response.result.value
                        completion(json as! [String : AnyObject])
                    break
                case .failure(_):
                     FTIndicator.showToastMessage("Server not responding")
                    break
                }
            }
        
    }
    

    
    // MARK: UPLOADTOSERVER
    class func uploadTaskFor(URLString : String , parameters : [String : String] , file : UIImage?, showIndicator : Bool , completion: @escaping (_ success: [String : AnyObject]) -> Void){
        Alamofire.upload(
            multipartFormData: { MultipartFormData in
                for (key, value) in parameters {
                    MultipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
                
                if file != nil{
                     MultipartFormData.append(UIImageJPEGRepresentation(file!, 1)!, withName: "myFile", fileName: "myFile.jpg", mimeType: "image/jpg")
                }
                
//                MultipartFormData.append(UIImageJPEGRepresentation(UIImage(named: "1.png")!, 1)!, withName: "photos[2]", fileName: "swift_file.jpeg", mimeType: "image/jpeg")
                
                
        }, to: URLString) { (result) in
            switch(result) {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    let json = response.result.value
                    if json != nil{
                        completion(json as! [String : AnyObject])
                    }else{
                        FTIndicator.showToastMessage("Server Error")
                    }
                }
                break;
            case .failure(_):
                 FTIndicator.showToastMessage("Server not responding")
                break;
            }
        }
        
        
    }
    
    class func uploadProfileImage(URLString : String , header : String , file : UIImage,  completion: @escaping (_ success: [String : AnyObject]) -> Void){
        let headers: HTTPHeaders = [
            "Authorization": header,
            "Accept": "application/json"
        ]
        Alamofire.upload(multipartFormData:{ multipartFormData in
            multipartFormData.append(UIImageJPEGRepresentation(file, 1)!, withName: "picture", fileName: "picture.png", mimeType: "image/png")},
                         usingThreshold:UInt64.init(),
                         to:URLString,
                         method:.post,
                         headers:headers,
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseJSON { response in
                                    completion(["Status" : "Completed" as AnyObject])
                                }
                            case .failure(let encodingError):
                                print(encodingError)
                                FTIndicator.showToastMessage("Error  Uploading Image")
                            }
        })
            
    }
    // MARK: GETFROMSERVER
    class func getResponseFromURLWithBlock(urlString: String,  completion: @escaping (_ data: [String : AnyObject]?, _ sucess:Bool?) -> Void){
       
        Alamofire.request(urlString).responseJSON { response in
            var data: [String : AnyObject]?
            var sucess = false
            switch response.result {
                case .success:
                    sucess = true
                    data = response.result.value as? [String : AnyObject]
                case .failure(let error):
                print(error)
                
                
            }
            completion(data,sucess)
        }
    }
  
}
